


# Test Project
The Test Project project provides a skeleton go project directory
with GitLab CI pipeline, go/webservice, and go/logging built in.
  

## Informations

### Version

0.0.0

### License

[Textkernel CONFIDENTIAL](http://tkgit-01.textkernel.net/pasterkamp/test-project/-/blob/master/LICENSE.txt)

### Contact

  http://tkgit-01.textkernel.net/pasterkamp/test-project

## Content negotiation

### URI Schemes
  * http

### Consumes
  * application/json

### Produces
  * application/octet-stream
  * text/html
  * application/json
  * text/text
  * text/plain

## All endpoints

###  debug

| Method  | URI     | Name   | Summary |
|---------|---------|--------|---------|
| GET | /debug/block | [blocking](#blocking) | debug/block |
| GET | /debug/profile | [CPU profile](#cpu-profile) | debug/profile |
| GET | /debug/cmdline | [command line](#command-line) | debug/cmdline |
| GET | /debug/goroutine | [go routines](#go-routines) | debug/goroutine |
| GET | /debug/allocs | [memory allocations](#memory-allocations) | debug/allocs |
| GET | /debug/heap | [memory heap](#memory-heap) | debug/heap |
| GET | /debug/mutex | [mutexes](#mutexes) | debug/mutex |
| GET | /debug/threadcreate | [new o s threads](#new-o-s-threads) | debug/threadcreate |
| GET | /debug/pprof | [profiling](#profiling) | debug/pprof |
| GET | /debug/symbol | [symbols](#symbols) | debug/symbol |
| GET | /debug/trace | [trace](#trace) | debug/trace |
  


###  kubernetes

| Method  | URI     | Name   | Summary |
|---------|---------|--------|---------|
| GET | /health | [health probe](#health-probe) | health |
| GET | /metrics | [prometheus metrics](#prometheus-metrics) | metrics |
| GET | /readiness | [readiness probe](#readiness-probe) | readiness |
| GET | /version | [version info](#version-info) | version |
  


###  main

| Method  | URI     | Name   | Summary |
|---------|---------|--------|---------|
| GET | /api/hello | [hello world](#hello-world) | api/hello |
  


## Paths

### <span id="blocking"></span> debug/block (*Blocking*)

```
GET /debug/block
```

Stack traces that led to blocking on synchronization primitives.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#blocking-200) | OK | binary data specific to the debug end-point |  | [schema](#blocking-200-schema) |

#### Responses


##### <span id="blocking-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="blocking-200-schema"></span> Schema
   
  



### <span id="cpu-profile"></span> debug/profile (*CPU-Profile*)

```
GET /debug/profile
```

Profile responds with the pprof-formatted cpu profile. Profiling lasts for duration specified in seconds GET parameter, or for 30 seconds if not specified.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#cpu-profile-200) | OK | binary data specific to the debug end-point |  | [schema](#cpu-profile-200-schema) |

#### Responses


##### <span id="cpu-profile-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="cpu-profile-200-schema"></span> Schema
   
  



### <span id="command-line"></span> debug/cmdline (*Command-Line*)

```
GET /debug/cmdline
```

Returns the running program's command line, with arguments separated by NUL bytes.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * text/text

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#command-line-200) | OK | Command line arguments used to start the application |  | [schema](#command-line-200-schema) |

#### Responses


##### <span id="command-line-200"></span> 200 - Command line arguments used to start the application
Status: OK

###### <span id="command-line-200-schema"></span> Schema
   
  



### <span id="go-routines"></span> debug/goroutine (*Go-Routines*)

```
GET /debug/goroutine
```

Stack traces of all current goroutines.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#go-routines-200) | OK | binary data specific to the debug end-point |  | [schema](#go-routines-200-schema) |

#### Responses


##### <span id="go-routines-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="go-routines-200-schema"></span> Schema
   
  



### <span id="health-probe"></span> health (*Health-Probe*)

```
GET /health
```

Always returns a 200 status code if the service is running.

#### URI Schemes
  * http

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#health-probe-200) | OK | Empty response |  | [schema](#health-probe-200-schema) |

#### Responses


##### <span id="health-probe-200"></span> 200 - Empty response
Status: OK

###### <span id="health-probe-200-schema"></span> Schema
   
  



### <span id="hello-world"></span> api/hello (*HelloWorld*)

```
GET /api/hello
```

Returns a simple hello-world response.

#### URI Schemes
  * http

#### Produces
  * text/plain

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#hello-world-200) | OK | HelloWorldResponse |  | [schema](#hello-world-200-schema) |

#### Responses


##### <span id="hello-world-200"></span> 200 - HelloWorldResponse
Status: OK

###### <span id="hello-world-200-schema"></span> Schema
   
  

| Name | Type | Go type | Default | Description | Example |
|------|------|---------| ------- |-------------|---------|
| helloWorldOKBody | string|  | |  |  |



### <span id="memory-allocations"></span> debug/allocs (*Memory-Allocations*)

```
GET /debug/allocs
```

A sampling of all past memory allocations.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * text/plain

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#memory-allocations-200) | OK | Heap profile |  | [schema](#memory-allocations-200-schema) |

#### Responses


##### <span id="memory-allocations-200"></span> 200 - Heap profile
Status: OK

###### <span id="memory-allocations-200-schema"></span> Schema
   
  



### <span id="memory-heap"></span> debug/heap (*Memory-Heap*)

```
GET /debug/heap
```

A sampling of memory allocations of live objects. You can specify the
gc GET parameter to run GC before taking the heap sample.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#memory-heap-200) | OK | binary data specific to the debug end-point |  | [schema](#memory-heap-200-schema) |

#### Responses


##### <span id="memory-heap-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="memory-heap-200-schema"></span> Schema
   
  



### <span id="mutexes"></span> debug/mutex (*Mutexes*)

```
GET /debug/mutex
```

Stack traces of holders of contended mutexes.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#mutexes-200) | OK | binary data specific to the debug end-point |  | [schema](#mutexes-200-schema) |

#### Responses


##### <span id="mutexes-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="mutexes-200-schema"></span> Schema
   
  



### <span id="new-o-s-threads"></span> debug/threadcreate (*New-OS-Threads*)

```
GET /debug/threadcreate
```

traces that led to the creation of new OS threads.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#new-o-s-threads-200) | OK | binary data specific to the debug end-point |  | [schema](#new-o-s-threads-200-schema) |

#### Responses


##### <span id="new-o-s-threads-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="new-o-s-threads-200-schema"></span> Schema
   
  



### <span id="profiling"></span> debug/pprof (*Profiling*)

```
GET /debug/pprof
```

Returns an HTML page listing the available profiles.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * text/html

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#profiling-200) | OK | Profiling HTML landing page |  | [schema](#profiling-200-schema) |

#### Responses


##### <span id="profiling-200"></span> 200 - Profiling HTML landing page
Status: OK

###### <span id="profiling-200-schema"></span> Schema
   
  



### <span id="prometheus-metrics"></span> metrics (*Prometheus-Metrics*)

```
GET /metrics
```

Returns Prometheus compatible metrics.

#### URI Schemes
  * http

#### Produces
  * text/plain

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#prometheus-metrics-200) | OK | Prometheus compatible metrics |  | [schema](#prometheus-metrics-200-schema) |

#### Responses


##### <span id="prometheus-metrics-200"></span> 200 - Prometheus compatible metrics
Status: OK

###### <span id="prometheus-metrics-200-schema"></span> Schema
   
  



### <span id="readiness-probe"></span> readiness (*Readiness-Probe*)

```
GET /readiness
```

Always returns a 200 status code if the service is ready to receive traffic once started, or 503 Service not Available if the service has not started yet or is in the process of shutting down gracefully.

#### URI Schemes
  * http

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#readiness-probe-200) | OK | Empty response |  | [schema](#readiness-probe-200-schema) |
| [503](#readiness-probe-503) | Service Unavailable | Service not Available response |  | [schema](#readiness-probe-503-schema) |

#### Responses


##### <span id="readiness-probe-200"></span> 200 - Empty response
Status: OK

###### <span id="readiness-probe-200-schema"></span> Schema
   
  



##### <span id="readiness-probe-503"></span> 503 - Service not Available response
Status: Service Unavailable

###### <span id="readiness-probe-503-schema"></span> Schema
   
  



### <span id="symbols"></span> debug/symbol (*Symbols*)

```
GET /debug/symbol
```

Symbol looks up the program counters listed in the request,
responding with a table mapping program counters to function
names.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * text/plain

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#symbols-200) | OK | table mapping program counters to function names |  | [schema](#symbols-200-schema) |

#### Responses


##### <span id="symbols-200"></span> 200 - table mapping program counters to function names
Status: OK

###### <span id="symbols-200-schema"></span> Schema
   
  



### <span id="trace"></span> debug/trace (*Trace*)

```
GET /debug/trace
```

A trace of execution of the current program. You can specify the duration in the seconds GET parameter. After you get the trace file, use the go tool trace command to investigate the trace.

This end-point is only available if profiling has been enabled.

#### URI Schemes
  * http

#### Produces
  * application/octet-stream

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#trace-200) | OK | binary data specific to the debug end-point |  | [schema](#trace-200-schema) |

#### Responses


##### <span id="trace-200"></span> 200 - binary data specific to the debug end-point
Status: OK

###### <span id="trace-200-schema"></span> Schema
   
  



### <span id="version-info"></span> version (*Version-Info*)

```
GET /version
```

Returns the version and build information of the application.

#### URI Schemes
  * http

#### Produces
  * application/json

#### All responses
| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [default](#version-info-default) | | Info |  | [schema](#version-info-default-schema) |

#### Responses


##### <span id="version-info-default"></span> Default Response
Info

###### <span id="version-info-default-schema"></span> Schema

  

[Info](#info)

## Models

### <span id="cause"></span> Cause


  



**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| Description | string| `string` | ✓ | | Description detailing the specific cause of the error. | `The Preprocessor URL is invalid: '//localhost'` |
| ID | string| `string` | ✓ | | ID is the error identifier unique within the current response scope. This can be used to refer to a specific attribute causing the offense. | `step.0.url` |
| URL | string| `string` |  | | Help is an optional URL-formatted reference to online documentation specific to the error. | `http://developer.textkernel.net/Product/master/Configuration#steps` |
| detail | [TranslatableDescription](#translatable-description)| `TranslatableDescription` |  | |  |  |



### <span id="error"></span> Error


> Error is a Textkernel Unified API Error structure. The format is meant for
returning errors to external customers, but can also be used for internal
error handling.
  





**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| Causes | [][Cause](#cause)| `[]*Cause` |  | | Causes is an optional list of accumulated causes contributing to the error, such as multiple validation violations. |  |
| Code | int64 (formatted integer)| `int64` | ✓ | | Code is a generic, numeric error code / HTTP Status Code. | `401` |
| Description | string| `string` | ✓ | | Description is a specific description detailing the cause of the error. | `The provided JWT token has expired` |
| ID | string| `string` | ✓ | | ID is the Textkernel-wide unique error code. | `401-jwt-expired-849bb44330d8` |
| Message | string| `string` | ✓ | | Message is the general, static error message corresponding to the code. | `Unauthorized` |
| TraceID | string| `string` | ✓ | | TraceID is an ID unique to the current request meant for tracing. | `664a953d-ab4a-460a-a182-d7f18b597785` |
| URL | string| `string` |  | | Help is an optional URL-formatted reference to online documentation specific to the error. | `http://developer.textkernel.net/Product/master/Authentication/#jwt-expired` |
| detail | [TranslatableDescription](#translatable-description)| `TranslatableDescription` |  | |  |  |
| severity | [Severity](#severity)| `Severity` |  | |  |  |



### <span id="hello-world-response"></span> HelloWorldResponse


  

| Name | Type | Go type | Default | Description | Example |
|------|------|---------| ------- |-------------|---------|
| HelloWorldResponse | string| string | |  |  |



### <span id="info"></span> Info


> in:body
  





**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| BuildTime | string| `string` | ✓ | | BuildTime is the timestamp when the binary was compiled. | `Fri Sep 16 11:21:11 UTC 2022` |
| Commit | string| `string` | ✓ | | Commit is the git sha from which the binary was compiled. | `c65b5e48` |
| Name | string| `string` | ✓ | | Name is the name of the running service | `Example Service` |
| Release | string| `string` | ✓ | | Release is the version of the running service | `1.0.0` |



### <span id="severity"></span> Severity


  



**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| Level | int64 (formatted integer)| `int64` | ✓ | | Level is a numeric indicator of the severity.
1 SeverityLevelTemporary  SeverityLevelTemporary is a Severity Level code indicating a temporary issue that is expected to be resolved automatically.
2 SeverityLevelRecoverable  SeverityLevelRecoverable is a Severity Level code indicating an issue is recoverable, but may need an automated or manual change before retrying.
3 SeverityLevelPermanent  SeverityLevelPermanent is a Severity Level code indicating an issue is not recoverable. Retrying will yield the same result. | `1` |
| Text | string| `string` | ✓ | | Text is the human readable description of the severity level.
Temporary SeverityDescriptionTemporary  SeverityDescriptionTemporary is a Severity Level code indicating a temporary issue that is expected to be resolved automatically. For example a '503 Service Unavailable' is typically temporary.
Recoverable SeverityDescriptionRecoverable  SeverityDescriptionRecoverable is a Severity Level code indicating an issue is recoverable, but may need an automated or manual change before retrying. For example a '401 Unauthorized' may require a new JWT token or using different credentials.
Permanent SeverityDescriptionPermanent  SeverityDescriptionPermanent is a Severity Level code indicating an issue is not recoverable. Retrying will yield the same result. For example oversized requests will always yield the same '413 Request Entity Too Large' response. | `\"Temporary error\` |



### <span id="translatable-description"></span> TranslatableDescription


> TranslatableDescription is a translatable error message template with
variable attributes containing the values.
  





**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| Description | string| `string` | ✓ | | Description is either the general, or singular form of the translatable message. | `The %{type}s URL has %{value}d issue: '%{url}s'.` |
| DescriptionPlural | string| `string` | ✓ | | DescriptionPlural is either the plural form of the same translatable message as the Description. The singular/plural form uses the numeric Value key. | `The %{type}s URL has %{value}d issues: '%{url}s'.` |
| Fields | map of any | `map[string]interface{}` |  | | Fields is a list of additional key-value pairs to be used in the description template. These keys are nested inline with all other fields so 'description', 'description_plural', and 'value' are reserved keys. | `{"type":"Preprocessor","url":"//localhost"}` |
| Value | int64 (formatted integer)| `int64` |  | | Value is a numeric indicator for using the singular or plural form. | `2` |


