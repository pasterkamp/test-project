# Contributing

## Standards

### Go Development Guidelines

This project follows the
[Go Development Guidelines](https://docs.google.com/document/d/1mZl33GonjHQEp9KYSxtS1M9f5gamWxMyOB6f_m_tPC4)
as described by the Architecture Team
([ARCH-116](https://jira.textkernel.nl/browse/ARCH-116)).

### Tech Stack Matrix

This project is included in the
[Go Tech Stack Matrix](https://docs.google.com/spreadsheets/d/1Xv0AWTHXGCyj2UCg8dIb-8rYRTLCeabDRr8S0un0HIc)
and (tries to) adhere to the tech stack choices as defined by the Architecture
Team ([ARCH-116](https://jira.textkernel.nl/browse/ARCH-116)).

## Useful commands

### Code formatting

Automatic code formatting:

```bash
gofmt -s -w .
```

Deeper code linting:

```bash
golangci-lint run
```

### Updating the swagger documentation

The swagger documentation can be generated on-demand using the following command:

```bash
go generate
```

Please install the go `swagger` command if the `go generate` command fails.

```bash
go install github.com/go-swagger/go-swagger/cmd/swagger@latest
```

### Building and running the app

```bash
docker build \
    --build-arg APP_BUILD_BY="${USER}" \
    --build-arg APP_VERSION="$(git describe master)" \
    --build-arg APP_COMMIT_SHORT_SHA="$(git rev-parse --short HEAD)" \
    -t tk/test-project .

docker run --rm -it -p 9090:9090 tk/test-project
```

### Building and running the documentation

For more information on writing and testing project documentation locally visit
the [Documentation engine](http://developer.textkernel.net/Documentation-engine/master/)
page.

To quickly test-render the documentation locally run:

```bash
# Make sure the target directory is owned by you, not the user dockerd is
# running as
mkdir -p $(pwd)/build

# Build the documentation
# Then launch the documentation on http://localhost/Local/dev/
docker run \
    --rm \
    -it \
    --user $(id -u):$(id -g) \
    --volume "$(pwd)/docs:/docs/docs/:ro" \
    --volume "$(pwd)/build:/build:rw" \
    registry-pull.textkernel.net/central/developers-docs:latest \
        python -u /app/build.py \
        --frail \
        --config /app/local.yml \
        --build /build/local \
&& docker run \
    --rm \
    -it \
    -p 80:80 \
    -v "$(pwd)/build/local:/usr/share/nginx/html:ro" \
    nginx:alpine
```

Then visit [localhost](http://localhost/Local/dev/contributing/) to view the
documentation.
