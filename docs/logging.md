The logging is implemented using the
[`tkgit-01.textkernel.net/go/logging`](http://tkgit-01.textkernel.net/go/logging/)
module. Please refer to the documentation for details on the configuration.

# Configuration examples

All logging messages go to the `root` logger by default. The access logs and
service (initialization/error) logs go to respective loggers as defined in the
configuration file. These three log streams can have different parameters,
including hook configurations for the various logging back-ends supported.

## Minimal STDOUT configuration

```logrus
root {
    level = "info"
    formatter.name = "text"
}
```

## Stream to fluent-bit side-car

For example:

```logrus
access {
    level = "info"
    formatter.name = "text"

    out.name = "rotatelogs"
    out.options {
        path = "access.%Y%m%d%H%M.log"
    }
}

root {
    level = "info"
    formatter.name = "json"

    hooks {
        logstash {
            endpoint = "localhost:5170"
        }
    }
}
```
