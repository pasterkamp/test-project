The service uses the basic webservice configuration:

```json
{
    "webservice": {
        "port": "9090",
        "profiling": true,
        "graceful": {
            "acknowledge_deadline": "30s",
            "shutdown_deadline": "5s"
        },
        "logging": {
            "webservice": "default",
            "access": "access"
        }
    },
    "logging_config": "logging.conf"
}
```

* `port` : The default port to host the service on.
* `profiling` : Enable the profiling end-points. Not recommended for production
   usage.
* `graceful` : Configuration parameters for a graceful shutdown procedure.
  * `acknowledge_deadline`: Limits how long a graceful shutdown will wait for
    at least one Kubernetes readiness probe to occur. This should exceed the
    `readinessProbe.periodSeconds` interval, so Kubernetes can stop routing
    traffic before the pod is actually terminated.

    This is a duration string. The default value is '0s', which disables this
    behavior.
  * `shutdown_deadline` : Is the time for internal contexts to finish up. This
    phase starts after either the `acknowledge_deadline` has exceeded, or the
    immanent shutdown was communicated by failing a readiness probe.

    For a Kubernetes deployment do not assume this period overlaps with the
    `acknowledge_deadline`, because it is not possible to determine when the
    readiness state has been checked and the change has officially been
    acknowledged.

    This is a duration string. The default value is '0s', which disables this
	behavior.
* `logging` : A configuration block to define which logging configuration to use.
  See [`tkgit-01.textkernel.net/go/logging`](https://tkgit-01.textkernel.net/go/logging/)
  for details.
  * `webservice` : The logging configuration to use for the webservice events,
    such as initialization, and shutdown notifications. The default value is
    `"default"`.
  * `access` : The logging configuration to use for the access log middleware.
    The default value is the same as `webservice`.

## Command line options

```none
Usage of ./main:
  -config string
        Path to JSON configuration file.
  -logging-config string
        Config file for the logging back-end. (default "logging.conf")
  -port string
        Port to listen on. (default "8080")
```
