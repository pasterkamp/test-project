/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package cli

import (
	"tkgit-01.textkernel.net/go/webservice"
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/build"

	"encoding/json"
	"flag"
	"fmt"
	"os"
)

// ParseArgs parses the command line arguments to create a webservice
// configuration object.
func ParseArgs(args []string) (Configuration, error) {
	cfg := Configuration{
		Webservice: webservice.Configuration{
			Port: "8080",
			Info: webservice.Info{
				Name:      build.Name,
				Release:   build.Version,
				Commit:    build.Commit,
				BuildTime: build.Date,
			},
			Logging: webservice.Logging{
				WebserviceLogger: "default",
				AccessLogger:     "access",
			},
		},
		LoggingConfig: "logging.conf",
	}

	// First pass of the arguments to get the configuration file path.
	cfgPath, err := parseArgs(args, &cfg)
	if err != nil {
		return cfg, err
	}

	// Update the values based on the provided configuration file.
	if cfgPath != "" {
		if err := readConfig(&cfg, cfgPath); err != nil {
			return cfg, err
		}
	}

	// Second pass of the arguments to have CLI arguments take priority over
	// the configuration file values.
	_, err = parseArgs(args, &cfg)
	if err != nil {
		return cfg, err
	}

	return cfg, nil
}

// parseArgs maps the argument strings to a configuration struct using the
// existing struct values as defaults.
func parseArgs(args []string, cfg *Configuration) (string, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("At least 1 argument needs to be passed (the executable)")
	}

	cfgPath := ""

	f := flag.NewFlagSet(args[0], flag.ExitOnError)
	f.StringVar(&cfg.Webservice.Port, "port", cfg.Webservice.Port, "Port to listen on.")
	f.StringVar(&cfg.LoggingConfig, "logging-config", cfg.LoggingConfig, "Config file for the logging back-end.")
	f.StringVar(&cfgPath, "config", cfgPath, "Path to JSON configuration file")
	if err := f.Parse(args[1:]); err != nil {
		return cfgPath, err
	}

	return cfgPath, nil
}

func readConfig(cfg *Configuration, path string) error {
	fh, err := os.Open(path)
	if err != nil {
		return err
	}
	defer fh.Close()

	err = json.NewDecoder(fh).Decode(&cfg)
	if err != nil {
		return err
	}

	return nil
}
