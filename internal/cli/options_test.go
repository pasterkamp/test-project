/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package cli_test

import (
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/build"
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/cli"

	"testing"
)

func TestParseArgs(t *testing.T) {
	var testCases = []struct {
		name               string
		args               []string
		expectedError      string
		expectedPort       string
		expectedProfiling  bool
		expectedLoggingCfg string
		expectedAccessLog  string
	}{
		{"Check default arguments", []string{"exe"},
			"", "8080", false, "logging.conf", "access"},
		{"Fail on bad args", []string{},
			"At least 1 argument needs to be passed (the executable)", "8080", false, "logging.conf", "access"},
		{"Fail on bad file", []string{"exe", "--config", "nonexistent.json"},
			"open nonexistent.json: no such file or directory", "", false, "logging.conf", "access"},
		{"Fail on malformed file", []string{"exe", "--config", "/dev/null"},
			"EOF", "", false, "logging.conf", "access"},
		{"Settings from config file", []string{"exe", "--config", "testdata/example.json"},
			"", "9090", true, "log.cfg", "request"},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			cfg, err := cli.ParseArgs(tt.args)

			if tt.expectedError != "" {
				if err.Error() != tt.expectedError {
					t.Errorf("Expected error %q, got %v.", tt.expectedError, err)
				}
				return
			}

			if err != nil {
				t.Errorf("Unexpected error: %v.", err)
			}

			if cfg.Webservice.Port != tt.expectedPort {
				t.Errorf("Unexpected port. Expected %q, got %q.",
					tt.expectedPort, cfg.Webservice.Port)
			}

			if cfg.Webservice.Profiling != tt.expectedProfiling {
				t.Errorf("Unexpected profiling. Expected %v, got %v.",
					tt.expectedProfiling, cfg.Webservice.Profiling)
			}

			if cfg.Webservice.Info.Name != build.Name {
				t.Errorf("Static build variables (cfg.Webservice.Info.Name) not passed. Expected %q, got %q.",
					build.Name, cfg.Webservice.Info.Name)
			}

			if cfg.LoggingConfig != tt.expectedLoggingCfg {
				t.Errorf("Unexpected logging configuration path. Expected %q, got %q.",
					tt.expectedLoggingCfg, cfg.LoggingConfig)
			}

			if cfg.Webservice.Logging.AccessLogger != tt.expectedAccessLog {
				t.Errorf("Unexpected access log value. Expected %q, got %q.",
					tt.expectedAccessLog, cfg.Webservice.Logging.AccessLogger)
			}
		})
	}
}
