/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package cli

import (
	"tkgit-01.textkernel.net/go/webservice"
)

// Configuration contains all parameters to start the main service, plus the
// shared go/webservice and go/logging configuration.
type Configuration struct {
	Webservice webservice.Configuration `json:"webservice"`
	// LoggingConfig is the path to the logging configuration. The default value
	// is logging.conf. For more details see tkgit-01.textkernel.net/go/logging.
	LoggingConfig string `json:"logging_config"`
}
