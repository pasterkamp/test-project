/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package build

var (
	// Name defines the application name
	Name = "Test Project"
	// Version defines the application version
	Version = "x.y.z"
	// Commit defines git short SHA commit
	Commit = "none"
	// Date defines when the app was built
	Date = "unknown"
	// By defines build by whom the app was built
	By = "unknown"
)
