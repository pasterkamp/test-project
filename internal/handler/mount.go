/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package handler

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
)

// Mount returns a go/chi router with the API end-points of the main service.
func Mount() http.Handler {
	r := chi.NewRouter()

	//swagger:route GET /api/hello Main HelloWorld
	//
	// /api/hello
	//
	// Returns a simple hello-world response.
	//
	//     Produces:
	//     - text/plain
	//
	//     Schemes: http
	//
	//     Responses:
	//       200: HelloWorldResponse
	r.Get("/hello", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprint(w, "Hello! Your request was processed.")
	})

	return r
}

// HelloWorldResponse is an example response used to demonstrate the microservice-template implementation.
//
//swagger:model HelloWorldResponse
type HelloWorldResponse string //nolint
