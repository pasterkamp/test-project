/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package handler_test

import (
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/handler"

	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMount(t *testing.T) {
	var testCases = []struct {
		name                string
		path                string
		method              string
		expectedStatus      int
		expectedContentType string
	}{
		{"Success on GET /hello", "/hello", "GET",
			http.StatusOK, "text/plain; charset=utf-8"},
		{"Fail on GET /nonexistent", "/nonexistent", "GET",
			http.StatusNotFound, "text/plain; charset=utf-8"},
	}
	r := handler.Mount()

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req := httptest.NewRequest(tt.method, tt.path, nil)

			r.ServeHTTP(rec, req)
			res := rec.Result()

			if res.StatusCode != tt.expectedStatus {
				t.Errorf("Unexpected Status code. Expected: %d, got: %d.",
					tt.expectedStatus, res.StatusCode)
			}

			contentType := res.Header.Get("Content-Type")
			if contentType != tt.expectedContentType {
				t.Errorf("Unexpected Content-Type. Expected %s, got %s.",
					tt.expectedContentType, contentType)
			}
		})
	}
}
