/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package main

import (
	"tkgit-01.textkernel.net/go/graceful"
	"tkgit-01.textkernel.net/go/logging"
	"tkgit-01.textkernel.net/go/webservice"
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/build"
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/cli"
	"tkgit-01.textkernel.net/pasterkamp/test-project/internal/handler"

	"fmt"
	"os"
)

func main() {
	cfg, err := cli.ParseArgs(os.Args)
	if err != nil {
		fmt.Printf("Failed to load configuration: %s", err)
		os.Exit(1)
	}

	err = logging.Init(cfg.LoggingConfig)
	if err != nil {
		fmt.Printf("Failed to load logging configuration from %q: %s",
			cfg.LoggingConfig, err)
		os.Exit(1)
	}

	hostname, err := os.Hostname()
	if err != nil {
		hostname = "localhost"
	}
	logging.AddFields(logging.Fields{
		logging.ServiceName:     build.Name,
		logging.ServiceVersion:  build.Version,
		logging.ServiceNodeName: hostname,
	})
	log := logging.GetLogger("default")

	graceful.SetLogger(log)

	ws, err := webservice.New(cfg.Webservice)
	if err != nil {
		log.Fatalf("Failed to initialize webserver: %s", err)
	}

	err = ws.Launch()
	if err != nil {
		log.Fatalf("Failed to launch webserver: %s", err)
	}

	// Warning: Do not mount on /
	// This will replace all existing end-points, like /version, /metrics, etc.
	ws.Router.Mount("/api", handler.Mount())

	ws.Ready()
	ws.BlockUntilShutdown()
}
