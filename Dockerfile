# Produces the go binary
FROM --platform=$BUILDPLATFORM golang:1.21 as build

WORKDIR /app

ENV GOPRIVATE=tkgit-01.textkernel.net
ENV GOFLAGS=-mod=vendor
ENV CGO_ENABLED=0

ADD . .

ARG APP_BUILD_BY=unknown
ARG APP_VERSION=unknown
ARG APP_COMMIT_SHORT_SHA=unknown

ENV ENV_BUILD_BY=${APP_BUILD_BY}
ENV ENV_GIT_COMMIT_SHORT_SHA=${APP_COMMIT_SHORT_SHA}
ENV ENV_VERSION=${APP_VERSION}

ARG TARGETOS
ARG TARGETARCH

RUN GOOS=$TARGETOS GOARCH=$TARGETARCH \
    go install -ldflags="\
    -s -w \
    -X 'tkgit-01.textkernel.net/pasterkamp/test-project/internal/build.Commit=${ENV_GIT_COMMIT_SHORT_SHA}' \
    -X 'tkgit-01.textkernel.net/pasterkamp/test-project/internal/build.Version=${ENV_VERSION}' \
    -X 'tkgit-01.textkernel.net/pasterkamp/test-project/internal/build.Date=$(date)' \
    -X 'tkgit-01.textkernel.net/pasterkamp/test-project/internal/build.By=${ENV_BUILD_BY}'" \
    cmd/test-project/main.go

# Ensures the CA certificates are available, and a non-root user exists
FROM --platform=$BUILDPLATFORM golang:1.21 AS security

RUN apt-get update && \
    apt-get install -y \
        ca-certificates

ARG UID=1000
ARG GID=1000
ARG USER=app
RUN addgroup \
        --system \
        --gid $GID \
        $USER \
    && adduser \
        --system \
        --uid $UID \
        --gid $GID \
        --disabled-password \
        --no-create-home \
        --home /opt \
        $USER \
    && chown $USER.$USER /opt

# Ensures the image is as small as possible
FROM scratch

COPY --from=security /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=security /etc/passwd /etc/passwd

# Do not use $USER :
# Error: container has runAsNonRoot and image has non-numeric user (app),
#        cannot verify user is non-root
USER $UID

WORKDIR /opt/

COPY --from=build /go/bin/main app

ENTRYPOINT [ "/opt/app" ]
