/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package webservice

import (
	"tkgit-01.textkernel.net/go/graceful"
	"tkgit-01.textkernel.net/go/webservice/middleware"

	"net/http"
)

// Configuration provides parameters to the webservice instance.
type Configuration struct {
	// Port is the port number the webservice should listen on. This parameter
	// is required.
	Port string `json:"port"`

	// Profiling enable/disables the /pprof/* endpoints. Defaults to false.
	Profiling bool `json:"profiling"`

	// Info is the data served by the /version endpoint. This should be
	// constructed using variables injected at build time, not parsed from a
	// configuration file.
	Info Info

	// Middleware is a list of middleware functions added to all routes. Use an
	// empty list of no middleware should be added. When leaving this nil the
	// default middleware will be used: traceid, prometheus, and logger. See
	// tkgit-01.textkernel.net/go/webservice/middleware for a list of available
	// middlewares.
	Middleware *[]func(http.Handler) http.Handler

	// Graceful is configuration used to initialize the graceful shutdown module
	// (http://developer.textkernel.net/Graceful/master). A non-nil value will
	// be used to call the graceful.Initialize(...) function.
	Graceful *graceful.Configuration `json:"graceful"`

	// ShutdownManager defaults to the go/graceful module
	// (http://developer.textkernel.net/Graceful/master).
	// Use go/graceful/mock instances to create graceful shutdown scenarios when
	// testing.
	ShutdownManager graceful.OrchestratingFinalizer

	// Logging is the configuration for the logging back-end.
	// See go/logging (http://developer.textkernel.net/Logging/master/) for more
	// details.
	Logging Logging `json:"logging,omitempty"`

	// OpenTelemetry is the configuration for the tracing platform.
	OpenTelemetry middleware.OpenTelemetry `json:"tracer,omitempty"`

	// Compression configures the response compression behavior.
	Compression Compression `json:"compression"`

	// MaxEntityBytes limits the size of the POST payload to protect the service
	// from oversized requests, and even zip-bombs. Setting this to 0 means no
	// limit. The default is 0.
	MaxEntityBytes int64 `json:"maxEntityBytes"`
}

// Logging defines which go/logging configuration should be used for the main
// webservice logging, and which ones for the access logs.
type Logging struct {
	// WebserviceLogger is the logger configuration name, and the main logger
	// used for the webservice events, such as initialization and shutdown
	// notifications. If not specified the default value is "default".
	WebserviceLogger string `json:"webservice,omitempty"`
	// AccessLogger is the logger configuration name used for the access log
	// middleware. If not specified the default value is the same as the
	// webservice logger name.
	AccessLogger string `json:"access,omitempty"`
}

// Compression is the configuration block to configure the response compression
// behavior.
type Compression struct {
	// Level defines the gzip compression level ranging from 1 to 9. Values
	// outside this range default to 5.
	Level int `json:"level"`

	// Types is a list of content-types that should be compressed. This list
	// extends the list of default content-types.
	Types []string `json:"types"`
}
