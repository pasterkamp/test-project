/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package webservice

import (
	"tkgit-01.textkernel.net/go/graceful"
	"tkgit-01.textkernel.net/go/logging"

	"net"
	"net/http"
	"sync/atomic"

	"github.com/go-chi/chi/v5"
)

// Webservice provides an easy HTTP Handler containing all end-points required
// to run a go service in Kubernetes.
type Webservice struct {
	// Router is the chi.Mux, so extra end-points can be added.
	Router   *chi.Mux
	info     Info
	port     string
	listener net.Listener
	server   *http.Server
	isReady  *atomic.Value
	graceful graceful.OrchestratingFinalizer
	log      *logging.Entry
}

// Info is the structured version data to be returned by the version handler.
//
//in:body
//swagger:model Info
type Info struct {
	// BuildTime is the timestamp when the binary was compiled.
	//
	//required: true
	//example: Fri Sep 16 11:21:11 UTC 2022
	BuildTime string `json:"build_time"`
	// Commit is the git sha from which the binary was compiled.
	//
	//required: true
	//example: c65b5e48
	Commit string `json:"commit"`
	// Name is the name of the running service
	//
	//required: true
	//example: Example Service
	Name string `json:"name"`
	// Release is the version of the running service
	//
	//required: true
	//example: 1.0.0
	Release string `json:"release"`
}
