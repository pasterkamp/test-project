/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package webservice

import (
	"net/http"
	"net/http/pprof"

	"github.com/go-chi/chi/v5"
)

// mountPProf returns a router object with all debug profiling end-points
func mountPProf() http.Handler {
	router := chi.NewRouter()

	router.Route("/", func(r chi.Router) {
		//swagger:route GET /debug/pprof Debug Profiling
		//
		// /debug/pprof
		//
		// Returns an HTML page listing the available profiles.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     - text/html
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: debugProfile
		r.Get("/pprof", pprof.Index)
		//swagger:route GET /debug/cmdline Debug Command-Line
		//
		// /debug/cmdline
		//
		// Returns the running program's command line, with arguments separated by NUL bytes.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     - text/text
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: debugCmdline
		r.Get("/cmdline", pprof.Cmdline)
		//swagger:route GET /debug/profile Debug CPU-Profile
		//
		// /debug/profile
		//
		// Profile responds with the pprof-formatted cpu profile. Profiling lasts for duration specified in seconds GET parameter, or for 30 seconds if not specified.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     - application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Get("/profile", pprof.Profile)
		//swagger:route GET /debug/symbol Debug Symbols
		//
		// /debug/symbol
		//
		// Symbol looks up the program counters listed in the request,
		// responding with a table mapping program counters to function
		// names.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     - text/plain
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: debugSymbol
		r.Get("/symbol", pprof.Symbol)
		//swagger:route GET /debug/trace Debug Trace
		//
		// /debug/trace
		//
		// A trace of execution of the current program. You can specify the duration in the seconds GET parameter. After you get the trace file, use the go tool trace command to investigate the trace.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     - application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Get("/trace", pprof.Trace)

		//swagger:route GET /debug/allocs Debug Memory-Allocations
		//
		// /debug/allocs
		//
		// A sampling of all past memory allocations.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     - text/plain
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: debugAllocs
		r.Handle("/allocs", pprof.Handler("allocs"))
		//swagger:route GET /debug/block Debug Blocking
		//
		// /debug/block
		//
		// Stack traces that led to blocking on synchronization primitives.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     -  application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Handle("/block", pprof.Handler("block"))
		//swagger:route GET /debug/goroutine Debug Go-Routines
		//
		// /debug/goroutine
		//
		// Stack traces of all current goroutines.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     -  application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Handle("/goroutine", pprof.Handler("goroutine"))
		//swagger:route GET /debug/heap Debug Memory-Heap
		//
		// /debug/heap
		//
		// A sampling of memory allocations of live objects. You can specify the
		// gc GET parameter to run GC before taking the heap sample.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     -  application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Handle("/heap", pprof.Handler("heap"))
		//swagger:route GET /debug/mutex Debug Mutexes
		//
		// /debug/mutex
		//
		// Stack traces of holders of contended mutexes.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     -  application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Handle("/mutex", pprof.Handler("mutex"))
		//swagger:route GET /debug/threadcreate Debug New-OS-Threads
		//
		// /debug/threadcreate
		//
		// traces that led to the creation of new OS threads.
		//
		// This end-point is only available if profiling has been enabled.
		//
		//     Produces:
		//     -  application/octet-stream
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: binary
		r.Handle("/threadcreate", pprof.Handler("threadcreate"))
	})

	return router
}

// Profiling HTML landing page
//
//swagger:response debugProfile
type debugProfile string //nolint

// Command line arguments used to start the application
//
//swagger:response debugCmdline
type debugCmdline string //nolint

// table mapping program counters to function names
//
//swagger:response debugSymbol
type debugSymbol string //nolint

// Heap profile
//
//swagger:response debugAllocs
type debugAllocs string //nolint

// binary data specific to the debug end-point
//
//swagger:response binary
type binary string //nolint
