/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package webservice

import (
	"encoding/json"
	"net/http"

	log "tkgit-01.textkernel.net/go/logging"
	"tkgit-01.textkernel.net/go/uapi"
	mw "tkgit-01.textkernel.net/go/webservice/middleware"
)

// ContentType is a header value to convey the mime type of the response.
const ContentType = "Content-Type"

// Success turns a structured object into a JSON response as the "result" field,
// with appropriate content-type and status code.
func Success(w http.ResponseWriter, _ *http.Request, result interface{}) error {
	w.Header().Set(ContentType, "application/json")

	switch {
	case result == nil:
		w.WriteHeader(http.StatusNoContent)
		return nil
	default:
		w.WriteHeader(http.StatusOK)
		return json.NewEncoder(w).Encode(map[string]interface{}{"result": result})
	}
}

// Warning turns a structured object into a JSON response with appropriate
// content-type, but adds a 'warning' attribute and sets the status code to the
// value of the warning Code.
func Warning(w http.ResponseWriter, r *http.Request, result interface{}, msg error) error {
	w.Header().Set(ContentType, "application/json")

	warn := uapi.Error{
		Code:        http.StatusOK,
		Message:     http.StatusText(http.StatusOK),
		ID:          "200-warning",
		Description: msg.Error(),
		Err:         msg,
	}
	if uWarn := uapi.GetError(msg); uWarn != nil {
		warn = *uWarn
	}

	// Make sure the uapi.Error has the correct TraceID field
	if traceID, ok := r.Context().Value(mw.TraceID).(string); ok {
		warn = warn.WithTraceID(traceID)
	}

	log.GetLogger("default").WithFields(warn.LogFields()).Warning(warn)

	switch {
	case warn.Code > 0:
		w.WriteHeader(warn.Code)
	case result == nil:
		w.WriteHeader(http.StatusNoContent)
	default:
		w.WriteHeader(http.StatusOK)
	}

	wrapped := struct {
		Result  interface{} `json:"result,omitempty"`
		Warning *uapi.Error `json:"warning,omitempty"`
	}{
		Result:  result,
		Warning: &warn,
	}

	return json.NewEncoder(w).Encode(wrapped)
}

// Error turns a structured object into a JSON response with appropriate
// content-type, but adds an 'error' attribute and sets the status code to the
// value of the error Code.
func Error(w http.ResponseWriter, r *http.Request, msg error) error {
	w.Header().Set(ContentType, "application/json")

	err := uapi.Error{
		Code:        http.StatusInternalServerError,
		Message:     http.StatusText(http.StatusInternalServerError),
		ID:          "500-error",
		Description: msg.Error(),
		Err:         msg,
	}
	if uErr := uapi.GetError(msg); uErr != nil {
		err = *uErr
	}

	// Make sure the uapi.Error has the correct TraceID field
	if traceID, ok := r.Context().Value(mw.TraceID).(string); ok {
		err = err.WithTraceID(traceID)
	}

	log.GetLogger("default").WithFields(err.LogFields()).Error(err)

	switch {
	case err.Code > 0:
		w.WriteHeader(err.Code)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}

	wrapped := struct {
		// Warning is a uapi.Error pointer in case generating the payload
		// encountered recoverable issues.
		Error *uapi.Error `json:"error,omitempty"`
	}{
		Error: &err,
	}

	return json.NewEncoder(w).Encode(wrapped)
}
