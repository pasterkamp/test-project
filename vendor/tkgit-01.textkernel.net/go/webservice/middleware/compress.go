/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"net/http"

	chi "github.com/go-chi/chi/v5/middleware"
)

// DefaultCompressibleContentTypes is a list of content-types that will be
// compressed by the Compress middleware.
// This is an exported version of the default list found in the
// go-chi/chi/middleware/compress.go file.
var DefaultCompressibleContentTypes = []string{
	"text/*",
	"application/javascript",
	"application/x-javascript",
	"application/json",
	"application/atom+xml",
	"application/rss+xml",
	"image/svg+xml",
}

// WithCompression is a middleware that compresses response body of a given
// content types to a data format based on Accept-Encoding request header. It
// uses a given compression level.
//
// NOTE: make sure to set the Content-Type header on your response
// otherwise this middleware will not compress the response body. For example
// your handler you should set
// w.Header().Set("Content-Type", http.DetectContentType(yourBody))
// or set it manually.
func WithCompression(level int, types ...string) func(next http.Handler) http.Handler {
	if level < 1 || level > 9 {
		level = 5
	}
	types = append(DefaultCompressibleContentTypes, types...)
	compressor := chi.NewCompressor(level, types...)
	return compressor.Handler
}
