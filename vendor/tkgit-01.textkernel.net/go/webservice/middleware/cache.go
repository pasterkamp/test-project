/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"bytes"
	"net/http"

	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	cacheHitCounter   prometheus.Counter
	cacheMissCounter  prometheus.Counter
	cacheEntryCounter prometheus.Counter
)

func init() {
	cacheHitCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "go_webservice_cache_middleware_hit_count",
			Help: "Counter of webservice.middleware cache hits.",
		},
	)
	cacheMissCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "go_webservice_cache_middleware_miss_count",
			Help: "Counter of webservice.middleware cache misses.",
		},
	)
	cacheEntryCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "go_webservice_cache_middleware_entry_count",
			Help: "Counter of webservice.middleware cache entries. This can be different " +
				"from go_webservice_cache_middleware_miss_count because two concurrent " +
				"threads can miss the cache and generate the same entry twice.",
		},
	)

	prometheus.MustRegister(cacheHitCounter)
	prometheus.MustRegister(cacheMissCounter)
	prometheus.MustRegister(cacheEntryCounter)
}

// WithCache is a middleware that adds a caching interceptor to a GET end-point.
// Cache hits will not call the next http.Handler, but return the cached
// status code and body instead. Cache misses will have the status code an
// response body stored in the cache for the next call. The CacheAdapter
// implementation is responsible for invalidating the cache.
func WithCache(c CacheAdapter) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			key := c.Key(r)
			if item, ok := c.Get(key); ok {
				cacheHitCounter.Inc()

				w.WriteHeader(item.status)
				_, _ = w.Write(item.body)
				return
			}

			cacheMissCounter.Inc()

			// to collect the status code
			crw := cachingResponseWriter{
				ResponseWriter: w,
				buf:            &bytes.Buffer{},
				status:         http.StatusOK,
			}

			// store the status and body in the cache once the request is done,
			// but only successful responses.
			defer func() {
				c.Put(key, CachedResponse{
					status: crw.status,
					body:   crw.buf.Bytes(),
				})
				cacheEntryCounter.Inc()
			}()

			next.ServeHTTP(&crw, r)
		})
	}
}

// CacheAdapter is an interface to a response cache implementation, plus
// a key generator function and an initialization function.
type CacheAdapter interface {
	// Delete deletes the value for a key.
	Delete(key interface{})
	// Get returns the value stored in the cache for a key, or nil if no value
	// is present. The ok result indicates whether value was found in the cache.
	Get(key interface{}) (item CachedResponse, ok bool)
	// Put sets the value for a key.
	Put(key interface{}, item CachedResponse)
	// Key returns a deterministic value based on the request. For a GET request
	// this may be the full URI /path?query.
	Key(r *http.Request) interface{}
}

// CachedResponse is a combination of the Response status code and the Response
// body.
type CachedResponse struct {
	status int
	body   []byte
}

type cachingResponseWriter struct {
	http.ResponseWriter
	buf    *bytes.Buffer
	status int
}

func (crw *cachingResponseWriter) WriteHeader(code int) {
	crw.status = code
	crw.ResponseWriter.WriteHeader(code)
}

func (crw *cachingResponseWriter) Write(b []byte) (int, error) {
	_, _ = crw.buf.Write(b)
	return crw.ResponseWriter.Write(b)
}

// memoryCache is an in memory cache implementation of the CacheAdapter. The
// store uses a sync.Map and stored items are enrichted with a Time to Live
// metadata.
type memoryCache struct {
	store sync.Map
	ttl   time.Duration
}

// memoryItem combines a response (status + body) with an expiration timestamp.
type memoryItem struct {
	item   CachedResponse
	expire time.Time
}

// Delete removes the cached key from the sync.Map.
func (m *memoryCache) Delete(key interface{}) {
	m.store.Delete(key)
}

// Get returns the value stored in the sync.Map.Load, and either invalidates
// expired values or returns the CachedResponse.
func (m *memoryCache) Get(key interface{}) (CachedResponse, bool) {
	value, ok := m.store.Load(key)
	if !ok {
		return CachedResponse{}, false
	}

	cache := value.(memoryItem)
	if time.Now().After(cache.expire) {
		m.store.Delete(key)
		return CachedResponse{}, false
	}

	return cache.item, true
}

// Store overloads the sync.Map.Store function to add an expiration timestmap to
// the cached response (status + body).
func (m *memoryCache) Put(key interface{}, item CachedResponse) {
	m.store.Store(key, memoryItem{
		expire: time.Now().Add(m.ttl),
		item:   item,
	})
}

// Key generates a deterministic value from a request. This implementation
// returns the request URI to cache GET requests.
func (m *memoryCache) Key(r *http.Request) interface{} {
	return r.URL.RequestURI()
}

// NewMemoryCache returns a memory (sync.Map) based implementation of the
// CacheAdapter interface. A background routine is started to prune expired
// items from memory. This prune loop terminates once the quit channel is closed.
func NewMemoryCache(ttl, pruneCycle time.Duration, quit <-chan interface{}) CacheAdapter {
	c := memoryCache{
		ttl: ttl,
	}

	ticker := time.NewTicker(pruneCycle)
	go func() {
		for {
			select {
			case <-ticker.C:
				c.store.Range(func(key, value interface{}) bool {
					cache, valid := value.(memoryItem)
					if valid && time.Now().After(cache.expire) {
						c.Delete(key)
					}
					return false
				})
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()

	return &c
}
