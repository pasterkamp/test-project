/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

// From https://www.robustperception.io/prometheus-middleware-for-gorilla-mux

package middleware

import (
	"context"
	"math/rand"
	"net/http"
	"time"

	uuid "github.com/google/uuid"
	"go.opentelemetry.io/otel/trace"

	"tkgit-01.textkernel.net/go/logging"
)

func init() {
	t := time.Now().UTC()
	entropy := rand.New(rand.NewSource(t.UnixNano()))
	uuid.SetRand(entropy)
}

// TraceIDHeader defines a W3C trace-context compatible standard request
// header to specify a trace ID.
// See https://w3c.github.io/trace-context/#trace-id
const TraceIDHeader = "trace-id"

// XRequestIDHeader defines a commonly used request ID header
// See https://en.wikipedia.org/wiki/List_of_HTTP_header_fields
const XRequestIDHeader = "X-Request-ID"

// XCorrelationIDHeader defines a commonly used alternative request ID header
// See https://en.wikipedia.org/wiki/List_of_HTTP_header_fields
const XCorrelationIDHeader = "X-Correlation-ID"

// WithTraceID is a middelware that adds a TraceID to the request's context.
// Use this middleware as soon as possible.
func WithTraceID() func(next http.Handler) http.Handler {
	logging.AddFields(logging.Fields{
		logging.TracingTraceID: TraceID,
	})

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			var id string

			if t := trace.SpanContextFromContext(ctx).TraceID(); t.IsValid() {
				id = t.String()
			} else if t := r.Header.Get(TraceIDHeader); t != "" {
				id = t
			} else if t := r.Header.Get(XRequestIDHeader); t != "" {
				id = t
			} else if t := r.Header.Get(XCorrelationIDHeader); t != "" {
				id = t
			} else {
				id = uuid.New().String()
			}

			ctx = context.WithValue(ctx, TraceID, id)
			w.Header().Add(TraceIDHeader, id)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
