/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"fmt"
	"net/http"

	"tkgit-01.textkernel.net/go/logging"
	"tkgit-01.textkernel.net/go/uapi"
)

// WithRecovery handles unexpected panic situations, logs the issue, and returns
// an Internal Server error.
func WithRecovery(logger logging.LogProvider) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				err := recover()
				if err != nil {
					traceID, _ := r.Context().Value(TraceID).(string)
					uErr := ErrRecovered.
						WithTraceID(traceID).
						Wrap(fmt.Errorf("%v", err)).(uapi.Error)

					logger().
						WithRequestFields(r).
						WithFields(uErr.LogFields()).
						Errorf("%v: %v", uErr, err)

					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(uErr.Code)
					_ = uErr.WriteError(w)
				}
			}()

			next.ServeHTTP(w, r)
		})
	}
}
