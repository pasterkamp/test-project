/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

// From https://github.com/joe-elliott/tempo-otel-example/blob/master/main.go

package middleware

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.opentelemetry.io/otel/trace"

	"tkgit-01.textkernel.net/go/graceful"
	"tkgit-01.textkernel.net/go/logging"
)

// TracerProtocol is an enum type of string
type TracerProtocol string

const (
	// ProtoDisabled is an empty string indicating tracing is disabled
	ProtoDisabled TracerProtocol = ""
	// ProtoGRCP specifies the GRPC otel exporter protocol
	ProtoGRCP TracerProtocol = "grpc"
	// ProtoSTDOUT specifies the STDOUT otel exporter protocol (for debugging)
	ProtoSTDOUT TracerProtocol = "stdout"
)

// OpenTelemetry defines json serializable configuration for the OpenTelemetry
// tracing platform.
type OpenTelemetry struct {
	// URL is the host:port of the OpenTelemetry compatible service to send
	// trace information to.
	URL string `json:"url"`
	// Insecure signals if TLS is disabled for the port.
	Insecure bool `json:"insecure,omitempty"`
	// Protocol defines which transport to be used. Supported protocols are
	// grpc, and stdout (for debugging).
	TracerProtocol `json:"protocol"`
	// Graceful overrides the default graceful shutdown usage
	Graceful graceful.Orchestrator
}

var (
	// ErrUnsupportedProtocol is the error returned when an unsupported, or
	// empty OpenTelemetry service protocol is configured.
	ErrUnsupportedProtocol = fmt.Errorf("unsupported OpenTelemetry protocol")
	// ErrExporterFailed is the error returned when the exporter did not
	//  initialize properly.
	ErrExporterFailed = fmt.Errorf("initializing exporter failed")
	// ErrResourceFailed is the error returned when the resource did not get
	//  created properly.
	ErrResourceFailed = fmt.Errorf("defining resource failed")
)

// Init connects to the OpenTelemetry service and prepares the module for
// collecting tracing information.
func (cfg OpenTelemetry) Init(service, version string) error {
	ctx := context.Background()

	var exp sdktrace.SpanExporter
	var err error

	switch cfg.TracerProtocol {
	case ProtoSTDOUT:
		exp, err = stdouttrace.New()
	case ProtoGRCP:
		opts := []otlptracegrpc.Option{otlptracegrpc.WithEndpoint(cfg.URL)}
		if cfg.Insecure {
			opts = append(opts, otlptracegrpc.WithInsecure())
		}
		exp, err = otlptracegrpc.New(ctx, opts...)
	case ProtoDisabled:
		return nil
	default:
		return fmt.Errorf("%w: %q", ErrUnsupportedProtocol, cfg.TracerProtocol)
	}
	if err != nil {
		return fmt.Errorf("%w: %w", ErrExporterFailed, err)
	}

	logging.GetLogger("default").Infof("Initialized %q tracer on %q for %q@%q.",
		cfg.TracerProtocol, cfg.URL, service, version)

	res, err := resource.New(ctx,
		resource.WithAttributes(
			semconv.ServiceNameKey.String(service),
			semconv.ServiceVersion(version),
		),
	)
	if err != nil {
		return fmt.Errorf("%w: %w", ErrResourceFailed, err)
	}

	bsp := sdktrace.NewBatchSpanProcessor(exp)

	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithResource(res),
		sdktrace.WithSpanProcessor(bsp),
	)

	// set global propagator to tracecontext (the default is no-op).
	otel.SetTextMapPropagator(propagation.TraceContext{})
	otel.SetTracerProvider(tracerProvider)

	go func() {
		graceful.AddParticipant()
		defer graceful.ParticipantDone()
		<-graceful.IsTerminating()

		// Shutdown will flush any remaining spans.
		err := tracerProvider.Shutdown(context.Background())
		if err != nil {
			logging.GetLogger("default").Error(err)
		}
	}()

	return nil
}

// WithOpenTelemetry sends telemetry metrics, if configured.
func WithOpenTelemetry(cfg OpenTelemetry) func(next http.Handler) http.Handler {
	// Don't collect telemetry when not configured
	if cfg.TracerProtocol == ProtoDisabled {
		return func(next http.Handler) http.Handler {
			return next
		}
	}

	return func(next http.Handler) http.Handler {
		tp := otel.GetTracerProvider()
		tracer := tp.Tracer("tkgit-01.textkernel.net/go/webservice/middleware")

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			wr := recordingResponseWriter{ResponseWriter: w}

			ctx, span := tracer.Start(
				r.Context(), fmt.Sprintf("%s %s", r.Method, r.URL.Path),
				trace.WithSpanKind(trace.SpanKindServer),
			)
			defer span.End()

			r = r.WithContext(ctx)

			defer func() {
				// set span route
				routePattern := chi.RouteContext(r.Context()).RoutePattern()
				span.SetAttributes(semconv.HTTPRouteKey.String(routePattern))

				// set span name
				spanName := fmt.Sprintf("%s %s", r.Method, routePattern)
				span.SetName(spanName)

				// set status code attribute
				span.SetAttributes(semconv.HTTPStatusCodeKey.Int(wr.status))

				// set span status
				code := codes.Unset
				if wr.status >= 200 && wr.status < 400 {
					code = codes.Ok
				} else if wr.status >= 400 {
					code = codes.Error
				}
				span.SetStatus(code, http.StatusText(wr.status))
			}()

			next.ServeHTTP(&wr, r)
		})
	}
}
