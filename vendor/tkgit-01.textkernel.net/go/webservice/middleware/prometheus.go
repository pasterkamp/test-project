/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

// From https://www.robustperception.io/prometheus-middleware-for-gorilla-mux

package middleware

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// WithPrometheus returns a function that implements mux.MiddlewareFunc.
func WithPrometheus(name, version string) func(next http.Handler) http.Handler {
	duration, _ := prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "go_webservice_request_duration_seconds",
			Help:    "Time (in seconds) spent serving HTTP requests.",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"service_name", "service_version", "method", "route", "status"},
	).CurryWith(prometheus.Labels{
		"service_name":    name,
		"service_version": version,
	})
	prometheus.Unregister(duration)
	_ = prometheus.Register(duration)

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			begin := time.Now()
			wr := recordingResponseWriter{ResponseWriter: w}

			defer func() {
				duration.With(prometheus.Labels{
					"method": r.Method,
					"route":  r.URL.Path,
					"status": wr.statusCodeString(),
				}).Observe(time.Since(begin).Seconds())
			}()

			next.ServeHTTP(&wr, r)
		})
	}
}
