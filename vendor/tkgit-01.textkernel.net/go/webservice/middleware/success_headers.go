/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"net/http"
)

type successHeaderResponseWriter struct {
	http.ResponseWriter
	wroteHeader bool
	header      http.Header
}

// WithHeadersOnSuccess writes one or more provided headers, but only if the
// response is a success (200 <= status < 300).
func WithHeadersOnSuccess(header http.Header) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			shw := successHeaderResponseWriter{
				ResponseWriter: w,
				header:         header,
			}
			next.ServeHTTP(&shw, r)
		})
	}
}

func (w *successHeaderResponseWriter) WriteHeader(code int) {
	if code >= 200 && code < 300 {
		for key, values := range w.header {
			for _, value := range values {
				w.Header().Set(key, value)
			}
		}
	}
	w.wroteHeader = true
	w.ResponseWriter.WriteHeader(code)
}

func (w *successHeaderResponseWriter) Write(b []byte) (int, error) {
	if !w.wroteHeader {
		w.WriteHeader(http.StatusOK)
	}
	return w.ResponseWriter.Write(b)
}
