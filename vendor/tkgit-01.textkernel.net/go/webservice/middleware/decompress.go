/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"compress/flate"
	"compress/gzip"
	"io"
	"net/http"

	"tkgit-01.textkernel.net/go/logging"
	"tkgit-01.textkernel.net/go/uapi"
)

// WithDecompression is a middleware that decompresses a request transparently.
func WithDecompression(logger logging.LogProvider) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodGet || r.Method == http.MethodHead {
				next.ServeHTTP(w, r)
				return
			}

			body := r.Body
			var uErr uapi.Error

			encoding := r.Header.Get("Content-Encoding")
			switch encoding {
			case "gzip", "x-gzip":
				compressed, err := gzip.NewReader(body)
				if err != nil {
					uErr = ErrDecodingFailed.
						WithUpdateDescription(encoding).
						Wrap(err).(uapi.Error)
					break
				}

				body = &gzipReadCloser{
					body:       body,
					compressed: compressed,
				}
			case "deflate":
				body = flate.NewReader(body)
			case "", "identity":
			default:
				uErr = ErrUnsupportedEncoding.
					WithUpdateDescription(r.Header.Get("Content-Encoding"))
			}

			if uErr.Code != 0 {
				traceID, _ := r.Context().Value(TraceID).(string)
				uErr = uErr.WithTraceID(traceID)

				logger().
					WithRequestFields(r).
					WithFields(uErr.LogFields()).
					Errorf("%v: %v", uErr, uErr.Err)

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(uErr.Code)
				_ = uErr.WriteError(w)
				return
			}

			r.Body = body
			r.Header.Del("Content-Encoding")
			next.ServeHTTP(w, r)
		})
	}
}

type gzipReadCloser struct {
	body       io.ReadCloser
	compressed *gzip.Reader
}

func (rc *gzipReadCloser) Read(p []byte) (n int, err error) {
	return rc.compressed.Read(p)
}

func (rc *gzipReadCloser) Close() error {
	rc.compressed = nil
	return rc.body.Close()
}
