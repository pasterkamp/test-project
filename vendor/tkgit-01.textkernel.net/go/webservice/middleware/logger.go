/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"tkgit-01.textkernel.net/go/logging"

	"net/http"
	"time"
)

var lifecyclePath = map[string]bool{
	"/health":    true,
	"/metrics":   true,
	"/readiness": true,
}

// WithLogger adds a log line for every incoming request.
func WithLogger(logger logging.LogProvider) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			begin := time.Now()
			wr := recordingResponseWriter{ResponseWriter: w}

			defer func() {
				e := logger().WithRequestFields(r).WithFields(logging.Fields{
					logging.HTTPResponseBodyBytes:  wr.written,
					logging.HTTPResponseStatusCode: wr.status,
					logging.EventDuration:          time.Since(begin).Nanoseconds(),
				})

				if lifecyclePath[r.URL.Path] {
					e.Debug()
				} else {
					e.Info()
				}
			}()

			next.ServeHTTP(&wr, r)
		})
	}
}
