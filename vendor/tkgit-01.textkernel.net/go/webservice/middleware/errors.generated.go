/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package middleware

import (
	"tkgit-01.textkernel.net/go/uapi"
)

// All code generated below this comment will be generated from errors.yaml
//go:generate uapi-errors-generate --output $GOFILE errors.yaml

// ErrRecovered is returned when an unexpected error occurred
var ErrRecovered = uapi.Error{
	Code:        500,
	Message:     "Internal Server Error",
	ID:          "500-webservice-recovered-error",
	Description: "an unexpected error occurred",
	Severity:    uapi.SeverityPermanent,
	URL:         "http://developer.textkernel.net/Webservice/master/errors/#500-webservice-recovered-error",
}

// ErrUnsupportedEncoding is returned when the Content-Encoding %q is not supported
var ErrUnsupportedEncoding = uapi.Error{
	Code:        415,
	Message:     "Unsupported Media Type",
	ID:          "415-webservice-unsupported-encoding",
	Description: "the Content-Encoding %q is not supported",
	Severity:    uapi.SeverityRecoverable,
	URL:         "http://developer.textkernel.net/Webservice/master/errors/#415-webservice-unsupported-encoding",
}

// ErrDecodingFailed is returned when could not decode the %q Content-Encoding
var ErrDecodingFailed = uapi.Error{
	Code:        500,
	Message:     "Internal Server Error",
	ID:          "500-webservice-decoding-failed",
	Description: "could not decode the %q Content-Encoding",
	Severity:    uapi.SeverityPermanent,
	URL:         "http://developer.textkernel.net/Webservice/master/errors/#500-webservice-decoding-failed",
}
