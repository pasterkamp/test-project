/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

// Package webservice is a wrapper around the go-chi/chi (github.com/go-chi/chi)
// router framework. This framework is very compatible with the official go http
// package, but makes stacking routes and layering middleware very easy to setup.
//
// The go/webservice package offers convenient middleware for metrics, tracing,
// access logging, and more. In addition the expected Kubernetes end-points and
// behavior is provided by default, like graceful shutdown, health and readiness
// probe end-points, a /version and /metrics end-point, as well as an optional
// /debug/pprof/* set of profiling end-points.
package webservice

//go:generate gomarkdoc . ./middleware --output docs/usage.md

//go:generate uapi-errors-generate --format markdown --output docs/errors.md middleware/errors.yaml
