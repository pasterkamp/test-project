/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package webservice

import (
	"tkgit-01.textkernel.net/go/graceful"

	"encoding/json"
	"net/http"
	"sync/atomic"
)

// versionHandler returns a simple HTTP handler function which writes a json
// response detailing the version of the service.
func versionHandler(i Info) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		e := json.NewEncoder(w)
		_ = e.Encode(i)
	}
}

// healthHandler is a simple liveliness probe.
func healthHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}

// readinessHandler is a readiness probe. it will only return http.StatusOK when
// the isReady value is true, until the graceful shutdown has been initiated.
func readinessHandler(isReady *atomic.Value, g graceful.Orchestrator) http.HandlerFunc {
	// Tell the graceful shutdown to wait until we have communicated the shutdown
	// has started to Kubernetes by means of sending a not-ready-anymore response.
	g.AddParticipant()

	// Remember if we've handled a health check after terminating
	var ack int32

	return func(w http.ResponseWriter, _ *http.Request) {
		// We're not ready until the Webservice.Ready() has been called.
		if isReady == nil || !isReady.Load().(bool) {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
			return
		}

		// We're ready until the graceful package has picked up a terminate signal
		select {
		case <-g.IsTerminating():
			// Signal we're ready for the shutdown only once
			if atomic.CompareAndSwapInt32(&ack, 0, 1) {
				g.ParticipantDone()
			}
			http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
		default:
			w.WriteHeader(http.StatusOK)
		}
	}
}
