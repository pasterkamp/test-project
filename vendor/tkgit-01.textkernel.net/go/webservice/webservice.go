/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package webservice

import (
	"tkgit-01.textkernel.net/go/graceful"
	"tkgit-01.textkernel.net/go/logging"
	mw "tkgit-01.textkernel.net/go/webservice/middleware"

	"context"
	"fmt"
	"net"
	"net/http"
	"sync/atomic"

	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// New creates a Webservice instance with port and version info
func New(c Configuration) (*Webservice, error) {
	if c.ShutdownManager != nil && c.Graceful != nil {
		return nil, fmt.Errorf(
			"cannot pass graceful shutdown configuration with custom shutdown manager")
	}

	g := c.ShutdownManager
	if g == nil {
		g = graceful.Observer()

		if c.Graceful != nil {
			if err := graceful.Initialize(*c.Graceful); err != nil {
				return nil, err
			}
		}
	}

	if c.Logging.WebserviceLogger == "" {
		c.Logging.WebserviceLogger = "default"
	}
	if c.Logging.AccessLogger == "" {
		c.Logging.AccessLogger = c.Logging.WebserviceLogger
	}

	isReady := &atomic.Value{}
	isReady.Store(false)

	err := c.OpenTelemetry.Init(c.Info.Name, c.Info.Release)
	if err != nil {
		return nil, err
	}

	if c.Middleware == nil {
		access := logging.Factory{Name: c.Logging.AccessLogger}
		logger := logging.Factory{Name: "default"}

		c.Middleware = &[]func(http.Handler) http.Handler{
			mw.WithOpenTelemetry(c.OpenTelemetry),
			mw.WithPrometheus(c.Info.Name, c.Info.Release),
			mw.WithTraceID(),
			mw.WithLogger(access.Create),
			mw.WithCompression(c.Compression.Level, c.Compression.Types...),
			mw.WithDecompression(logger.Create),
			mw.WithLimitReader(c.MaxEntityBytes),
			mw.WithRecovery(logger.Create),
		}
	}

	router := chi.NewRouter()
	router.Use(*c.Middleware...)

	//swagger:route GET /version Kubernetes Version-Info
	//
	// /version
	//
	// Returns the version and build information of the application.
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http
	//
	//     Responses:
	//       default: Info
	router.Get("/version", versionHandler(c.Info))

	//swagger:route GET /health Kubernetes Health-Probe
	//
	// /health
	//
	// Always returns a 200 status code if the service is running.
	//
	//     Schemes: http
	//
	//     Responses:
	//       200: okResponse
	router.Get("/health", healthHandler)

	//swagger:route GET /readiness Kubernetes Readiness-Probe
	//
	// /readiness
	//
	// Always returns a 200 status code if the service is ready to receive traffic once started, or 503 Service not Available if the service has not started yet or is in the process of shutting down gracefully.
	//
	//     Schemes: http
	//
	//     Responses:
	//       200: okResponse
	//       503: notAvailableResponse
	router.Get("/readiness", readinessHandler(isReady, g))

	//swagger:route GET /metrics Kubernetes Prometheus-Metrics
	//
	// /metrics
	//
	// Returns Prometheus compatible metrics.
	//
	//     Produces:
	//     - text/plain
	//
	//     Schemes: http
	//
	//     Responses:
	//       200: prometheusMetrics
	router.Handle("/metrics", promhttp.Handler())

	if c.Profiling {
		router.Mount("/debug", mountPProf())
	}

	return &Webservice{
		info:     c.Info,
		server:   &http.Server{Handler: router},
		port:     c.Port,
		Router:   router,
		isReady:  isReady,
		graceful: g,
		log:      logging.GetLogger(c.Logging.WebserviceLogger),
	}, nil
}

// Launch will start the web service on the port, and serves the configured
// route handlers.
func (w *Webservice) Launch() (err error) {
	if w.listener != nil {
		return fmt.Errorf("webservice is already running")
	}

	w.log.Infof("Starting %s on %s: Version %s, Commit %s, Build Time %s",
		w.info.Name, w.URL(), w.info.Release, w.info.Commit, w.info.BuildTime)

	w.listener, err = net.Listen("tcp", ":"+w.port)
	if err != nil {
		return
	}

	go func() {
		err := w.server.Serve(w.listener)
		if err != http.ErrServerClosed {
			w.log.Errorf("Web server failed to start: %v", err)
		}
	}()

	return
}

// Ready signals that the service is ready to serve. Call once all initialization
// has completed.
func (w *Webservice) Ready() {
	w.isReady.Store(true)
}

// BlockUntilShutdown will block until a graceful shutdown has started. This
// will gracefully shutdown the webserver, then exit.
func (w Webservice) BlockUntilShutdown() {
	<-w.graceful.IsShutdown()

	ctx, cancel := w.graceful.ShutdownContext()
	defer cancel()

	w.Shutdown(ctx)
}

// Shutdown gracefully terminates the service. Automatically called when
// receiving SIGINT, or SIGTERM.
func (w Webservice) Shutdown(ctx context.Context) {
	err := w.server.Shutdown(ctx)
	if err != nil {
		w.log.Errorf("Unclean web server Shutdown: %v", err)
	}
}

// URL returns the address:port on which the service is listening.
func (w Webservice) URL() string {
	if w.listener == nil {
		return "http://localhost:" + w.port
	}
	return "http://" + w.listener.Addr().String()
}

// Prometheus compatible metrics
//
//swagger:response prometheusMetrics
type prometheusMetrics string //nolint

// Empty response
//
//swagger:response okResponse
type okResponse string //nolint

// Service not Available response
//
//swagger:response notAvailableResponse
type notAvailableResponse string //nolint
