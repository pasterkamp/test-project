/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package uapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"

	log "tkgit-01.textkernel.net/go/logging"
)

// Error returns the detailed error description, if available. Otherwise the
// generic error message is returned.
func (e Error) Error() string {
	if e.Description != "" {
		return e.Description
	}

	return e.Message
}

// LogMessage returns a formatted message with essential details filled in.
func (e Error) LogMessage(a ...any) string {
	format := ""
	args := []interface{}{}

	if e.ID == "" {
		format += "%d %s"
		args = append(args, e.Code, e.Message)
	} else {
		e = e.WithUpdateDescription(a...)
		format += "%s %s"
		args = append(args, e.ID, e.Description)
	}

	if e.Err != nil {
		format += ": %v"
		args = append(args, e.Err)
	}

	if e.URL != "" {
		format += " - for more information visit %s"
		args = append(args, e.URL)

	}

	return fmt.Sprintf(format, args...)
}

// GetError takes an error and finds the last uapi.Error returned. The uAPI
// Error may still be wrapping other errors which may or may not be of type
// uapi.Error. Can return nil if the error is not a uapi.Error or did not wrap
// any uAPI Error instances.
func GetError(err error) *Error {
	for {
		if err == nil {
			return nil
		}
		if uErr, ok := err.(Error); ok {
			return &uErr
		}
		err = errors.Unwrap(err)
	}
}

// Unwrap returns the original error this uAPI Error was wrapped around.
func (e Error) Unwrap() error {
	return e.Err
}

// Wrap returns the new uAPI Error with the original error embedded as context.
// The original error can be recovered using errors.Unwrap.
func (e Error) Wrap(err error) error {
	if err == nil {
		return nil
	}

	e.Err = err
	return e
}

// Is compares the error with a target error. If the original error has a unique
// ID attribute, then the target error must have the same ID. Otherwise it must
// have the same Code and Description. This function always returns false if the
// target is not a uapi.Error instance.
func (e Error) Is(target error) bool {
	err, ok := target.(Error)
	if !ok {
		// Not a uapi.Error
		return false
	}

	if e.ID != "" {
		// The same if they have the same Error.ID
		return err.ID == e.ID
	}

	// This is why uAPI Errors should have a unique ID
	return err.Code == e.Code && err.Error() == e.Error()
}

// Clone returns a copy of the error without modifying the original.
func (e Error) Clone() Error {
	return Error{
		Err:         e.Err,
		Code:        e.Code,
		Message:     e.Message,
		ID:          e.ID,
		Description: e.Description,
		TraceID:     e.TraceID,
		Detail:      e.Detail,
		Causes:      e.Causes,
		URL:         e.URL,
		Severity:    e.Severity,
	}
}

// WithTraceID returns a clone of the error with the TraceID filled in.
func (e Error) WithTraceID(traceID string) Error {
	c := e.Clone()
	c.TraceID = traceID
	return c
}

// WithUpdateDescription returns a clone of the error with the Description
// arguments filled in. The description of the Error must be a formatting string.
// The arguments are handled by fmt.Sprintf.
func (e Error) WithUpdateDescription(a ...any) Error {
	c := e.Clone()
	c.Description = fmt.Sprintf(c.Description, a...)
	return c
}

// Clone returns a copy of the sub error without modifying the original.
func (s Cause) Clone() Cause {
	return Cause{
		ID:          s.ID,
		Description: s.Description,
		Detail:      s.Detail,
		URL:         s.URL,
	}
}

// WithUpdateDescription returns a clone of the error with the Description arguments
// filled in. The description of the Error must be a formatting string. The
// arguments are handled by fmt.Sprintf
func (s Cause) WithUpdateDescription(a ...any) Cause {
	c := s.Clone()
	c.Description = fmt.Sprintf(c.Description, a...)
	return c
}

// WriteError encodes the Error as JSON and writes this to the provided
// io.Writer in an object under the 'error' key. The JSON will look like:
// {"error": <uapi.Error>}
func (e Error) WriteError(w io.Writer) error {
	return json.NewEncoder(w).Encode(map[string]Error{"error": e})
}

// WriteWarning encodes the Error as JSON and writes this to the provided
// io.Writer in an object under the 'error' key. The JSON will look like:
// {"error": <uapi.Error>}
func (e Error) WriteWarning(w io.Writer) error {
	return json.NewEncoder(w).Encode(map[string]Error{"warning": e})
}

// LogFields returns a list of log fields mapped from the uapi.Error object.
func (e Error) LogFields() log.Fields {
	code := fmt.Sprintf("%d", e.Code)
	return log.Fields{
		log.TracingTraceID:         e.TraceID,
		"message":                  e.Description, // typically replaced by the `.Error(message)`
		log.ErrorID:                e.ID,
		log.ErrorCode:              code,
		log.ErrorMessage:           e.Message,
		log.HTTPResponseStatusCode: code,
	}
}
