/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package uapi

// Error is a Textkernel Unified API Error structure. The format is meant for
// returning errors to external customers, but can also be used for internal
// error handling.
//
//swagger:model UAPIError
type Error struct {
	// Err is the original Error context this error is wrapped around.
	Err error `json:"-"`
	// Code is a generic, numeric error code / HTTP Status Code.
	//
	//required: true
	//example: 401
	Code int `json:"code"`
	// Message is the general, static error message corresponding to the code.
	//
	//required: true
	//example: Unauthorized
	Message string `json:"message"`
	// ID is the Textkernel-wide unique error code.
	//
	//required: true
	//example: 401-jwt-expired-849bb44330d8
	ID string `json:"id"`
	// Description is a specific description detailing the cause of the error.
	//
	//required: true
	//example: The provided JWT token has expired
	Description string `json:"description"`
	// TraceID is an ID unique to the current request meant for tracing.
	//
	//required: true
	//example: 664a953d-ab4a-460a-a182-d7f18b597785
	TraceID string `json:"trace_id"`
	// Detail is a translatable error description intended for displaying in a User Interface.
	Detail *TranslatableDescription `json:"detail,omitempty"`
	// Causes is an optional list of accumulated causes contributing to the error, such as multiple validation violations.
	Causes []Cause `json:"causes,omitempty"`
	// Help is an optional URL-formatted reference to online documentation specific to the error.
	//
	//example: http://developer.textkernel.net/Product/master/Authentication/#jwt-expired
	URL string `json:"url,omitempty"`
	// Severity is an indication of effort required to resolve the error.
	//
	//example: Recoverable error
	Severity Severity `json:"severity,omitempty"`
}

// Cause describes an underlying issue contributing resulting in an error.
//
//swagger:model UAPIErrorCause
type Cause struct {
	// ID is the error identifier unique within the current response scope. This can be used to refer to a specific attribute causing the offense.
	//
	//required: true
	//example: step.0.url
	ID string `json:"id"`
	// Description detailing the specific cause of the error.
	//
	//required: true
	//example: The Preprocessor URL is invalid: '//localhost'
	Description string `json:"description"`
	// Detail is a translatable error description intended for displaying in a User Interface.
	Detail *TranslatableDescription `json:"detail,omitempty"`
	// Help is an optional URL-formatted reference to online documentation specific to the error.
	//
	//example: http://developer.textkernel.net/Product/master/Configuration#steps
	URL string `json:"url,omitempty"`
}

// TranslatableDescription is a translatable error message template with
// variable attributes containing the values.
//
//swagger:model UAPITranslatableDescription
type TranslatableDescription struct {
	// Description is either the general, or singular form of the translatable message.
	//
	//required: true
	//example: The %{type}s URL has %{value}d issue: '%{url}s'.
	Description string `json:"description"`
	// DescriptionPlural is either the plural form of the same translatable message as the Description. The singular/plural form uses the numeric Value key.
	//
	//required: true
	//example: The %{type}s URL has %{value}d issues: '%{url}s'.
	DescriptionPlural string `json:"description_plural"`
	// Value is a numeric indicator for using the singular or plural form.
	//
	//example: 2
	Value int `json:"value,omitempty"`
	// Fields is a list of additional key-value pairs to be used in the description template. These keys are nested inline with all other fields so 'description', 'description_plural', and 'value' are reserved keys.
	//
	//example: {"type": "Preprocessor", "url": "//localhost"}
	Fields map[string]interface{} `json:",inline,omitempty"`
}

// Severity is an indication of effort required to resolve the error.
//
//swagger:enum Severity
type Severity struct {
	// Level is a numeric indicator of the severity.
	//
	//required: true
	//example: 1
	Level SeverityLevel `json:"level"`
	// Text is the human readable description of the severity level.
	//
	//required: true
	//example: "Temporary error"
	Text SeverityDescription `json:"text"`
}

var (
	// SeverityTemporary is a predefined Severity structure for Temporary errors.
	SeverityTemporary = Severity{
		Level: SeverityLevelTemporary,
		Text:  SeverityDescriptionTemporary,
	}
	// SeverityRecoverable is a predefined Severity structure for Recoverable errors.
	SeverityRecoverable = Severity{
		Level: SeverityLevelRecoverable,
		Text:  SeverityDescriptionRecoverable,
	}
	// SeverityPermanent is a predefined Severity structure for Permanent errors.
	SeverityPermanent = Severity{
		Level: SeverityLevelPermanent,
		Text:  SeverityDescriptionPermanent,
	}
)

// SeverityLevel is an indication of effort required to resolve the error.
//
//swagger:enum SeverityLevel
type SeverityLevel int

const (
	// SeverityLevelTemporary is a Severity Level code indicating a temporary issue that is expected to be resolved automatically.
	SeverityLevelTemporary SeverityLevel = 1
	// SeverityLevelRecoverable is a Severity Level code indicating an issue is recoverable, but may need an automated or manual change before retrying.
	SeverityLevelRecoverable SeverityLevel = 2
	// SeverityLevelPermanent is a Severity Level code indicating an issue is not recoverable. Retrying will yield the same result.
	SeverityLevelPermanent SeverityLevel = 3
)

// SeverityDescription is an indication of effort required to resolve the error.
//
//swagger:enum SeverityDescription
type SeverityDescription string

const (
	// SeverityDescriptionTemporary is a Severity Level code indicating a temporary issue that is expected to be resolved automatically. For example a '503 Service Unavailable' is typically temporary.
	SeverityDescriptionTemporary SeverityDescription = "Temporary"
	// SeverityDescriptionRecoverable is a Severity Level code indicating an issue is recoverable, but may need an automated or manual change before retrying. For example a '401 Unauthorized' may require a new JWT token or using different credentials.
	SeverityDescriptionRecoverable SeverityDescription = "Recoverable"
	// SeverityDescriptionPermanent is a Severity Level code indicating an issue is not recoverable. Retrying will yield the same result. For example oversized requests will always yield the same '413 Request Entity Too Large' response.
	SeverityDescriptionPermanent SeverityDescription = "Permanent"
)
