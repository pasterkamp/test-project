/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package graceful

// Configuration is a struct used to Initialize the graceful package with.
type Configuration struct {
	// AcknowledgeDeadline limits how long a graceful shutdown will wait for
	// participants to acknowledge the termination signal. During this time the
	// shutdown is essentially ignored. This is useful for situations where the
	// imminent shutdown needs to be communicated to external components, like
	// a Kubernetes readiness probe. This is a duration string. The default
	// value is '0s', which disables this behavior.
	AcknowledgeDeadline string `json:"acknowledge_deadline"`
	// ShutdownDeadline is the time for internal contexts to finish up. This
	// phase starts after either the AcknowledgeDeadline has exceeded, or all
	// registered participants have acknowledged the termination signal. This
	// graceful shutdown period is useful for ending running threads. This value
	// is used to create the return values of the ShutdownContext() function,
	// which is useful for calling e.g. the Shutdown function on the http.Server.
	// This is a duration string. The default value is '0s', which disables this
	// behavior.
	ShutdownDeadline string `json:"shutdown_deadline"`
}
