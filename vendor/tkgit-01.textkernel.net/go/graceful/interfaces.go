/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package graceful

import (
	"context"
)

// Finalizer is an interface defining functions to finalize a graceful shutdown.
// The graceful package implements the Finalizer interface, but so does the
// graceful.mock.Finalizer which can be instantiated to test code depending on
// on the graceful package.
type Finalizer interface {
	// IsShutdown returns the channel that is closed once the shutdown sequence
	// has completed.
	IsShutdown() <-chan interface{}

	// ShutdownContext returns the context created when the final shutdown phase
	// has started, which ends after the shutdown deadline has exceeded.
	ShutdownContext() (context.Context, context.CancelFunc)
}

// Orchestrator is an interface defining functions to register graceful shutdown
// participants that would like to delay a final shutdown phase until they have
// acknowledged the termination signal.
// The graceful package implements the Orchestrator interface, but so does the
// graceful.mock.Orchestrator which can be instantiated to test code depending
// on the graceful package.
type Orchestrator interface {
	// IsTerminating returns the channel that is closed once a shutdown signal
	// has been received, but before the final shutdown has started.
	IsTerminating() <-chan interface{}

	// AddParticipant registers a graceful shutdown participant that may delay
	// the final shutdown phase.
	AddParticipant()

	// ParticipantDone signals that the graceful shutdown participant has
	// acknowledged the termination signal.
	ParticipantDone()
}

// OrchestratingFinalizer is an interface that encompasses all expected behavior
// of the graceful shutdown package. This is mainly used for dependency
// injection and mocking in tests.
type OrchestratingFinalizer interface {
	Orchestrator
	Finalizer
}
