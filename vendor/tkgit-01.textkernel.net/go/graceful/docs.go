/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

// Package graceful provides a complete package to handle graceful shutdown when
// a terminating signal (SIGINT, SIGQUIT, SIGTERM, SIGTSTP) has been received.
// The package allows for participants to register themselves as being
// interested in responding to the termination signal. Once the participants
// have had a chance to respond to the termination signal, a graceful period
// is started to let any remaining processes to wrap up.
package graceful

//go:generate gomarkdoc --output docs/usage.md
