/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package graceful

// Logger is the minimum requirement of graceful to inject a custom logger
type Logger interface {
	// Info is called to log normal graceful shutdown events without formatting
	// of variables.
	Info(args ...interface{})
	// Infof is called to log normal graceful shutdown events with formatting of
	// variables.
	Infof(format string, args ...interface{})
	// Warning is called to log graceful shutdown events that may result in a
	// less-than-graceful shutdown without formatting of variables.
	Warning(args ...interface{})
	// Warning is called to log graceful shutdown events that may result in a
	// less-than-graceful shutdown with formatting of variables.
	Warningf(format string, args ...interface{})
}

// NullLogger is an implementation of a Logger interface that doesn't do
// anything.
type NullLogger struct{}

// Info does not do anything; this is an implementation of the NullLogger.
func (n NullLogger) Info(_ ...interface{}) {
	// The null logger doesn't do anything
}

// Infof does not do anything; this is an implementation of the NullLogger.
func (n NullLogger) Infof(_ string, _ ...interface{}) {
	// The null logger doesn't do anything
}

// Warning does not do anything; this is an implementation of the NullLogger.
func (n NullLogger) Warning(_ ...interface{}) {
	// The null logger doesn't do anything
}

// Warningf does not do anything; this is an implementation of the NullLogger.
func (n NullLogger) Warningf(_ string, _ ...interface{}) {
	// The null logger doesn't do anything
}
