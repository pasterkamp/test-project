/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package graceful

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

// Observation is a graceful instance monitoring for signals and coordinating a
// graceful shutdown. The graceful package creates a global instance of this
// type, so local instances are only useful for testing purposes.
type Observation struct {
	// wait for all participants to inform us that they're done
	participants sync.WaitGroup
	// terminating will be closed when a signal has been received
	terminating chan interface{}
	// shutdown will be closed once all participants have finished
	shutdown chan interface{}
	// acknowledgeDeadline is the max time spent waiting for participants to
	// flag being ready for termination.
	acknowledgeDeadline time.Duration
	// shutdownDeadline is the max time spent waiting for internal contexts to
	// close.
	shutdownDeadline time.Duration
	// log is an implementation of the Logger interface. The default logger
	// is the NullLogger, which is silent.
	log Logger

	// ctxMu is the mutex to protect concurrent writes to the shutdown context
	ctxMu sync.Mutex
	// shutdownCtx is the context created to facilitate the final shutdown phase
	shutdownCtx context.Context
	// shutdownCancel is the cancel function created with the shutdownCtx
	shutdownCancel func()
}

// Initialize will register syscall observer to monitor signals. The
// Configuration allows for setting a non-default deadline for the graceful
// shutdown period.
func (o *Observation) Initialize(c Configuration) error {
	if c.AcknowledgeDeadline != "" {
		deadline, err := time.ParseDuration(c.AcknowledgeDeadline)
		if err != nil {
			return err
		}
		o.acknowledgeDeadline = deadline
	}

	if c.ShutdownDeadline != "" {
		deadline, err := time.ParseDuration(c.ShutdownDeadline)
		if err != nil {
			return err
		}
		o.shutdownDeadline = deadline
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGTSTP)
	go o.waitForSignal(sig)

	return nil
}

// SetLogger will replace the current logger with the one provided.
func (o *Observation) SetLogger(l Logger) {
	o.log = l
}

// Shutdown informs all graceful shutdown participants that the process is ending.
// The shutdown channel is closed once all participants are done. Returns
// false if the shutdown exceeded the deadline.
func (o *Observation) Shutdown() (success bool) {
	closeIfOpen(o.terminating)
	defer closeIfOpen(o.shutdown)

	start := time.Now()

	// sync.WaitGroup.Wait() does not work with 'select', but channels do.
	// with this construction we can block until either the waitgroup is done
	// or the deadline has exceeded.
	c := make(chan struct{})
	go func() {
		defer close(c)
		o.participants.Wait()
	}()

	select {
	case <-c:
		o.log.Infof("Graceful shutdown acknowledged after %v.", time.Since(start))
		success = true
	case <-time.After(o.acknowledgeDeadline):
		o.log.Warning("Graceful shutdown not yet acknowledged. Forcing shutdown.")
	}

	o.ctxMu.Lock()
	defer o.ctxMu.Unlock()
	o.shutdownCtx, o.shutdownCancel = context.WithTimeout(context.Background(), o.shutdownDeadline)

	o.log.Info("Ready to shutdown.")

	return
}

// IsTerminating returns the channel that is closed once a shutdown signal has
// been received, but before the final shutdown has started. This is the signal
// for all shutdown participants to wrap up and call ParticipantDone.
func (o *Observation) IsTerminating() <-chan interface{} {
	return o.terminating
}

// IsShutdown returns the channel that is closed once the shutdown phase has
// started. This channel is closed after the IsTerminating channel, and either
// after once all participants have closed up, or the termination deadline has
// exceeded.
func (o *Observation) IsShutdown() <-chan interface{} {
	return o.shutdown
}

// ShutdownContext returns the context created with a deadline set to the
// ShutdownDeadline duration once the Termination phase has finished. This
// context may be used once the shutdown phase has started to call other
// shutdown functions. A common use-case is to call the Shutdown function of a
// running web service instance once the graceful shutdown period has officially
// started.
func (o *Observation) ShutdownContext() (context.Context, context.CancelFunc) {
	o.ctxMu.Lock()
	defer o.ctxMu.Unlock()
	return o.shutdownCtx, o.shutdownCancel
}

// AddParticipant increments the participants WaitGroup. Please call
// 'defer ParticipantDone' immediately after adding the participant. Then wait
// for the IsTerminating() channel to close.
func (o *Observation) AddParticipant() {
	o.participants.Add(1)
}

// ParticipantDone decrements the participants WaitGroup.
func (o *Observation) ParticipantDone() {
	o.participants.Done()
}

func (o *Observation) waitForSignal(sig chan os.Signal) {
	defer o.Shutdown()
	val := <-sig
	o.log.Warningf("Received signal %v, initiating graceful shutdown.", val)
}
