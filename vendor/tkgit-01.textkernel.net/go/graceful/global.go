/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package graceful

import (
	"context"
	"sync"
	"time"
)

/**
 * This file contains functions to expose the module functions as package
 * functions. This ensures importing the package is enough to participate,
 * and signals are only observed by a single (global) instance.
 */

// g contains the global observation state for a shared graceful shutdown
var (
	g   *Observation
	gMu sync.Mutex
)

func init() {
	g = &Observation{
		terminating:         make(chan interface{}, 1),
		shutdown:            make(chan interface{}, 1),
		acknowledgeDeadline: 0 * time.Second,
		shutdownDeadline:    0 * time.Second,
		log:                 NullLogger{},
	}
}

// Initialize is identical to Observation.Initialize, but uses the global state
// transparently.
func Initialize(c Configuration) error {
	gMu.Lock()
	defer gMu.Unlock()
	return g.Initialize(c)
}

// Observer returns the global Observation instance that may be used to interact
// with the graceful package instead of the function wrappers. This can be used
// to inject the graceful shutdown handling dependency where the mock
// implementations are to be used during testing.
func Observer() OrchestratingFinalizer {
	return g
}

// SetLogger will replace the current logger with the one provided.
func SetLogger(l Logger) {
	g.SetLogger(l)
}

// ResetGlobalState clears the global observer state. Use this only for
// testing purposes.
func ResetGlobalState() {
	gMu.Lock()
	defer gMu.Unlock()
	g = &Observation{
		terminating:         make(chan interface{}, 1),
		shutdown:            make(chan interface{}, 1),
		acknowledgeDeadline: 0 * time.Second,
		shutdownDeadline:    0 * time.Second,
		log:                 NullLogger{},
	}
}

// Shutdown is identical to Observation.Shutdown, but uses the global state
// transparently.
func Shutdown() {
	g.Shutdown()
}

// ShutdownContext is identical to Observation.ShutdownContext, but uses the
// global shutdown state transparently.
func ShutdownContext() (context.Context, context.CancelFunc) {
	return g.ShutdownContext()
}

// IsTerminating is identical to Observation.IsTerminating, but uses the global
// shutdown state transparently.
func IsTerminating() <-chan interface{} {
	return g.IsTerminating()
}

// IsShutdown is identical to Observation.IsShutdown, but uses the global state
// transparently.
func IsShutdown() <-chan interface{} {
	return g.IsShutdown()
}

// AddParticipant is identical to Observation.AddParticipant, but uses the
// global state transparently.
func AddParticipant() {
	g.AddParticipant()
}

// ParticipantDone is identical to Observation.ParticipantDone, but uses the
// global state transparently.
func ParticipantDone() {
	g.ParticipantDone()
}
