/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package logging

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
)

// LogProvider is a function definition that returns fresh logging.Entry
// instances.
type LogProvider func() *Entry

// Factory is a log provider which returns new instances of the logger by the
// provided name.
type Factory struct {
	// Name is the logging stream in the configuration. Leaving this blank or
	// providing a non-existing log stream configuration results in creating a
	// default logger, or the logrus standard "root" logger if not configured.
	Name string
}

// Create returns a new logging.Entry using the log stream defined in the
// Factory.Name.
func (f Factory) Create() *Entry {
	return GetLogger(f.Name)
}

// GetLogger returns a logger by name, and injects all static fields by default.
// If there is no specific logger matching the name, the "default" logger is
// returned. If no default logger is specified, the logrus standard "root"
// logger is returned.
// In all cases the log entries are sent to the logrus standard logger anyway.
func GetLogger(name string) *Entry {
	var e Entry
	if mate == nil {
		e = Entry{
			Entry: logrus.StandardLogger().WithFields(logrus.Fields(logFields)),
		}
	} else if l := mate.Logger(name); l != nil {
		e = Entry{
			Entry: l.WithFields(logrus.Fields(logFields)),
		}
	} else if l := mate.Logger("default"); l != nil {
		e = Entry{
			Entry: l.WithFields(logrus.Fields(logFields)),
		}
	} else {
		e = Entry{
			Entry: logrus.StandardLogger().WithFields(logrus.Fields(logFields)),
		}
	}
	return &e
}

// WithFields adds static fields to the log message.
// Please consult the fields defined in the the Elastic Common Schema (ECS)
// (https://www.elastic.co/guide/en/ecs/current/ecs-field-reference.html)
// before adding fields. Most relevant fields have been defined as constant
// values for your convenience.
//
// Note: WithFields is not thread safe. Make sure a unique logger is created for
// the current thread using the logging.Factory.
func (e *Entry) WithFields(f Fields) *Entry {
	e.Entry = e.Entry.WithFields(logrus.Fields(f))
	return e
}

// WithContextFields injects fields to the log message as available in the
// context. See AddFields on how to configure logging on which fields to pick up
// from the context.
//
// Note: WithContextFields is not thread safe. Make sure a unique logger is
// created for the current thread using the logging.Factory.
func (e *Entry) WithContextFields(ctx context.Context) *Entry {
	f := Fields{}
	for logKey, ctxKey := range ctxFields {
		if v := ctx.Value(ctxKey); v != nil {
			f[logKey] = v
		}
	}
	return e.WithFields(f)
}

// WithRequestFields is for emitting request specific log entries. Injects
// traceID from the request context, and the version and nodeName variables from
// AddFields.
//
// Note: WithRequestFields is not thread safe. Make sure a unique logger is
// created for the current thread using the logging.Factory.
func (e *Entry) WithRequestFields(r *http.Request) *Entry {
	return e.WithContextFields(r.Context()).WithFields(Fields{
		HTTPRequestMethod:    r.Method,
		HTTPVersion:          r.Proto,
		HTTPRequestBodyBytes: r.ContentLength,
		URLOriginal:          r.URL.RequestURI(),
		URLDomain:            r.URL.Host,
		URLFragment:          r.URL.Fragment,
		URLPath:              r.URL.Path,
		URLQuery:             r.URL.RawQuery,
		URLScheme:            r.URL.Scheme,
	})
}

// WithResponseFields is for emitting request specific log entries. Injects
// traceID from the request context, and the version and nodeName variables from
// AddFields.
//
// Note: WithResponseFields is not thread safe. Make sure a unique logger is
// created for the current thread using the logging.Factory.
func (e *Entry) WithResponseFields(r *http.Response) *Entry {
	return e.WithFields(Fields{
		HTTPResponseStatusCode: r.StatusCode,
		HTTPVersion:            r.Proto,
		HTTPResponseBodyBytes:  r.ContentLength,
	})
}
