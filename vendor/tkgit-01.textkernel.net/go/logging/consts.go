/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package logging

// Base Fields
// The base field set contains all fields which are at the root of the events.
// These fields are common across all types of events.
const (
	// Labels are custom key/value pairs. Can be used to add meta information to
	// events. Should not contain nested objects. All values are stored as
	// keyword. Example: docker and k8s labels.
	Labels = "labels"
	// Tags is a list of keywords used to tag each event. Note: this field
	// should contain an array of values. Example: ["production", "env2"]
	Tags = "tags"
)

// Client Fields
// A client is defined as the initiator of a network connection for events
// regarding sessions, connections, or bidirectional flow records. The client is
// generally the initiator or requestor in the network transaction. The client
// fields describe details about the system acting as the client in the network
// event. Client fields are usually populated in conjunction with server fields.
// Client fields are generally not populated for packet-level events.
// Client / server representations can add semantic context to an exchange,
// which is helpful to visualize the data in certain situations. If your context
// falls in that category, you should still ensure that source and destination
// are filled appropriately.
const (
	// ClientAddress is the ambiguous client address. You should always store
	// the raw address in the .address field. Then it should be duplicated to
	// ClientIP or ClientDomain, depending on which one it is.
	ClientAddress = "client.address"
	// ClientBytes denotes the number of bytes sent from the client to the server.
	ClientBytes = "client.bytes"
	// ClientDomain is the domain name of the client. See ClientAddress.
	ClientDomain = "client.domain"
	// ClientIP is the IP address of the client (IPv4 or IPv6). See ClientAddress.
	ClientIP = "client.ip"
)

// Cloud Fields
// Fields related to the cloud or infrastructure the events are coming from.
const (
	// CloudAccountID is the cloud account or organization id used to identify
	// different entities in a multi-tenant environment. Examples: AWS account
	// id, Google Cloud ORG Id, or other unique identifier.
	CloudAccountID = "cloud.account.id"
	// CloudAccountName is the cloud account name or alias used to identify
	// different entities in a multi-tenant environment. Examples: AWS account
	// name, Google Cloud ORG display name.
	CloudAccountName = "cloud.account.name"
	// CloudAvailabilityZone is the availability zone in which this host,
	// resource, or service is located. Example: us-east-1c.
	CloudAvailabilityZone = "cloud.availability_zone"
	// CloudInstanceID is the instance ID of the host machine.
	// Example: i-1234567890abcdef0
	CloudInstanceID = "cloud.instance.id"
	// CloudInstanceName is the instance name of the host machine.
	CloudInstanceName = "cloud.instance.name"
	// CloudMachineType is the machine type of the host machine. Example: t2.medium
	CloudMachineType = "cloud.machine.type"
	// CloudProjectID is the cloud project identifier. Examples: Google Cloud
	// Project id, Azure Project id. Example: my-project
	CloudProjectID = "cloud.project.id"
	// CloudProjectName is the cloud project name. Examples: Google Cloud
	// Project name, Azure Project name. Example: my project
	CloudProjectName = "cloud.project.name"
	// CloudProvider is the name of the cloud provider. Example values are
	// aws, azure, gcp, or digitalocean. Example: aws
	CloudProvider = "cloud.provider"
	// CloudRegion is the region in which this host, resource, or service is
	// located. Example: us-east-1
	CloudRegion = "cloud.region"
	// CloudServiceName is the cloud service name is intended to distinguish
	// services running on different platforms within a provider, eg AWS EC2 vs
	// Lambda, GCP GCE vs App Engine, Azure VM vs App Server. Examples: app
	// engine, app service, cloud run, fargate, lambda.
	CloudServiceName = "cloud.service.name"
)

// Destination Fields
// Destination fields capture details about the receiver of a network exchange /
// packet. These fields are populated from a network event, packet, or other
// event containing details of a network transaction. Destination fields are
// usually populated in conjunction with source fields. The source and
// destination fields are considered the baseline and should always be filled if
// an event contains source and destination details from a network transaction.
// If the event also contains identification of the client and server roles,
// then the client and server fields should also be populated.
const (
	// DestinationAddress is the ambiguous destination address. The event will
	// sometimes list an IP, a domain or a unix socket. You should always store
	// the raw address in the DestinationAddress field. Then it should be
	// duplicated to DestinationIP or DestinationDomain, depending on which one
	// it is.
	DestinationAddress = "destination.address"
	// DestinationBytes is the number of bytes sent from the destination to the
	// source.
	DestinationBytes = "destination.bytes"
	// DestinationDomain is the destination domain name.
	DestinationDomain = "destination.domain"
	// DestinationIP is the IP address of the destination (IPv4 or IPv6).
	DestinationIP = "destination.ip"
	// DestinationPort is the port of the destination.
	DestinationPort = "destination.port"
)

// Error Fields
// These fields can represent errors of any kind. Use them for errors that
// happen while fetching events or in cases where the event itself contains an
// error.
const (
	// ErrorCode is the error code describing the error.
	ErrorCode = "error.code"
	// ErrorID is the unique identifier for the error.
	ErrorID = "error.id"
	// ErrorMessage is the error message.
	ErrorMessage = "error.message"
	// ErrorStackTrace the stack trace of this error in plain text.
	ErrorStackTrace = "error.stack_trace"
	// ErrorType The type of the error, for example the class name of the
	// exception. Example: java.lang.NullPointerException
	ErrorType = "error.type"
)

// Event Fields
// The event fields are used for context information about the log or metric
// event itself. A log is defined as an event containing details of something
// that happened. Log events must include the time at which the thing happened.
// Examples of log events include a process starting on a host, a network packet
// being sent from a source to a destination, or a network connection between a
// client and a server being initiated or closed. A metric is defined as an
// event containing one or more numerical measurements and the time at which the
// measurement was taken. Examples of metric events include memory pressure
// measured on a host and device temperature.
const (
	// EventAction the action captured by the event. This describes the
	// information in the event. It is more specific than event.category.
	// Examples are group-add, process-started, file-created. The value is
	// normally defined by the implementer. Example: user-password-change
	EventAction = "event.action"
	// EventCategory this is one of four ECS Categorization Fields, and
	// indicates the second level in the ECS category hierarchy. EventCategory
	// represents the "big buckets" of ECS categories. For example, filtering on
	// EventCategory:process yields all events relating to process activity.
	// This field is closely related to event.type, which is used as a
	// subcategory. This field is an array. This will allow proper
	// categorization of some events that fall in multiple categories.
	//
	// Note: this field should contain an array of values.
	//
	// Important: The field value must be one of the following:
	// authentication, configuration, database, driver, file, host, iam,
	// intrusion_detection, malware, network, package, process, registry,
	// session, threat, web
	EventCategory = "event.category"
	// EventCode identification code for this event, if one exists. Some event
	// sources use event codes to identify messages unambiguously, regardless
	// of message language or wording adjustments over time. An example of this
	// is the Windows Event ID.
	EventCode = "event.code"
	// EventDataset is the name of the dataset. If an event source publishes
	// more than one type of log or events (e.g. access log, error log), the
	// dataset is used to specify which one the event comes from. It’s
	// recommended but not required to start the dataset name with the module
	// name, followed by a dot, then the dataset name. Example: apache.access
	EventDataset = "event.dataset"
	// EventDuration is the duration of the event in nanoseconds. If EventStart
	// and EventEnd are known this value should be the difference between the
	// end and start time.
	EventDuration = "event.duration"
	// EventEnd contains the date when the event ended or when the activity was
	// last observed.
	EventEnd = "event.end"
	// EventHash hash (perhaps logstash fingerprint) of raw field to be able to
	// demonstrate log integrity.
	EventHash = "event.hash"
	// EventID is the unique ID to describe the event.
	EventID = "event.id"
	// EventKind is one of four ECS Categorization Fields, and indicates the
	// highest level in the ECS category hierarchy. EventKind gives high-level
	// information about what type of information the event contains, without
	// being specific to the contents of the event. For example, values of this
	// field distinguish alert events from metric events.
	//
	// The value of this field can be used to inform how these kinds of events
	// should be handled. They may warrant different retention, different access
	// control, it may also help understand whether the data coming in at a
	// regular interval or not.
	//
	// Important: The field value must be one of the following:
	// alert, enrichment, event, metric, state, pipeline_error, signal
	EventKind = "event.kind"
	// EventModule is the name of the module this data is coming from.
	// If your monitoring agent supports the concept of modules or plugins to
	// process events of a given source (e.g. Apache logs), event.module should
	// contain the name of this module. Example: apache.
	EventModule = "event.module"
	// EventOutcome is one of four ECS Categorization Fields, and indicates the
	// lowest level in the ECS category hierarchy. EventOutcome simply denotes
	// whether the event represents a success or a failure from the perspective
	// of the entity that produced the event.
	//
	// Note that when a single transaction is described in multiple events, each
	// event may populate different values of EventOutcome, according to their
	// perspective. Also note that in the case of a compound event (a single
	// event that contains multiple logical events), this field should be
	// populated with the value that best captures the overall success or
	// failure from the perspective of the event producer. Further note that
	// not all events will have an associated outcome. For example, this field
	// is generally not populated for metric events, events with EventType:info,
	// or any events for which an outcome does not make logical sense.
	//
	// Important: The field value must be one of the following:
	// failure, success, unknown
	EventOutcome = "event.outcome"
	// EventProvider is the source of the event.
	EventProvider = "event.provider"
	// EventReason is the reason why this event happened, according to the
	// source. This describes the why of a particular action or outcome captured
	// in the event. Where event.action captures the action from the event,
	// EventReason describes why that action was taken. For example, a web proxy
	// with an EventAction which denied the request may also populated
	// EventReason with the reason why (e.g. blocked site). Example: Terminated
	// an unexpected process
	EventReason = "event.reason"
	// EventReference is a reference URL linking to additional information about
	// this event. This URL links to a static definition of this event. Alert
	// events, indicated by EventKind:alert, are a common use case for this
	// field. Example: https://system.example.com/event/#0001234
	EventReference = "event.reference"
	// EventSequence is a sequence number of the event. The sequence number is a
	// value published by some event sources, to make the exact ordering of
	// events unambiguous, regardless of the timestamp precision.
	EventSequence = "event.sequence"
	// EventSeverity is the numeric severity of the event according to your
	// event source. What the different severity values mean can be different
	// between sources and use cases. It’s up to the implementer to make sure
	// severities are consistent across events from the same source.
	EventSeverity = "event.severity"
	// EventStart contains the date when the event started or when the activity
	// was first observed.
	EventStart = "event.start"
	// EventType is one of four ECS Categorization Fields, and indicates the
	// third level in the ECS category hierarchy. EventType represents a
	// categorization "sub-bucket" that, when used along with the EventCategory
	// field values, enables filtering events down to a level appropriate for
	// single visualization. This field is an array. This will allow proper
	// categorization of some events that fall in multiple event types.
	//
	// Note: this field should contain an array of values.
	//
	// Important: The field value must be one of the following:
	// access, admin, allowed, change, connection, creation, deletion, denied,
	// end, error, group, indicator, info, installation, protocol, start, user
	EventType = "event.type"
	// EventURL is a URL linking to an external system to continue investigation
	// of this event. This URL links to another system where in-depth
	// investigation of the specific occurrence of this event can take place.
	// Alert events, indicated by EventKind:alert, are a common use case for
	// this field.
	// Example: https://mysystem.example.com/alert/5271dedb-f5b0-4218-87f0-4ac4870a38fe
	EventURL = "event.url"
)

// Host Fields
// A host is defined as a general computing instance. ECS host.* fields should
// be populated with details about the host on which the event happened, or from
// which the measurement was taken. Host types include hardware, virtual
// machines, Docker containers, and Kubernetes nodes.
const (
	// HostArchitecture is the operating system architecture. Example: x86_64
	HostArchitecture = "host.architecture"
	// HostDomain is the name of the domain of which the host is a member. For
	// example, on Windows this could be the host’s Active Directory domain or
	// NetBIOS domain name. For Linux this could be the domain of the host’s
	// LDAP provider.
	HostDomain = "host.domain"
	// HostHostname is the hostname of the host. It normally contains what the
	// hostname command returns on the host machine.
	HostHostname = "host.hostname"
	// HostID is the unique host id. As hostname is not always unique, use
	// values that are meaningful in your environment. Example: The current
	// usage of beat.name.
	HostID = "host.id"
	// HostIP is the host ip addresses.
	// Note: this field should contain an array of values.
	HostIP = "host.ip"
	// HostName is the name of the host. It can contain what hostname returns on
	// Unix systems, the fully qualified domain name, or a name specified by the
	// user. The sender decides which value to use.
	HostName = "host.name"
	// HostType is the type of host. For Cloud providers this can be the machine
	// type like t2.medium. If vm, this could be the container, for example, or
	// other information meaningful in your environment.
	HostType = "host.type"
)

// HTTP Fields
// Fields related to HTTP activity. Use the url field set to store the url of
// the request.
const (
	// HTTPRequestBodyBytes is the size in bytes of the request body.
	HTTPRequestBodyBytes = "http.request.body.bytes"
	// HTTPRequestBodyContent is the full HTTP request body.
	HTTPRequestBodyContent = "http.request.body.content"
	// HTTPRequestBytes is the total size in bytes of the request (body and
	// headers).
	HTTPRequestBytes = "http.request.bytes"
	// HTTPRequestID is a unique identifier for each HTTP request to correlate
	// logs between clients and servers in transactions. The id may be contained
	// in a non-standard HTTP header, such as X-Request-ID or X-Correlation-ID.
	// Example: 123e4567-e89b-12d3-a456-426614174000
	HTTPRequestID = "http.request.id"
	// HTTPRequestMethod is the HTTP request method. Example: GET, POST, PUT,
	// PoST
	HTTPRequestMethod = "http.request.method"
	// HTTPRequestMimeType is the mime type of the body of the request. This
	// value must only be populated based on the content of the request body,
	// not on the Content-Type header. Comparing the mime type of a request with
	// the request’s Content-Type header can be helpful in detecting threats or
	// misconfigured clients. Example: image/gif
	HTTPRequestMimeType = "http.request.mime_type"
	// HTTPRequestReferrer is the referrer for this HTTP request.
	HTTPRequestReferrer = "http.request.referrer"
	// HTTPResponseBodyBytes is the size in bytes of the response body.
	HTTPResponseBodyBytes = "http.response.body.bytes"
	// HTTPResponseBodyContent is the full HTTP response body.
	// Example: Hello world
	HTTPResponseBodyContent = "http.response.body.content"
	// HTTPResponseBytes is the total size in bytes of the response (body and
	// headers).
	HTTPResponseBytes = "http.response.bytes"
	// HTTPResponseMimeType is the mime type of the body of the response. This
	// value must only be populated based on the content of the response body,
	// not on the Content-Type header. Comparing the mime type of a response
	// with the response’s Content-Type header can be helpful in detecting
	// misconfigured servers. Example: image/gif
	HTTPResponseMimeType = "http.response.mime_type"
	// HTTPResponseStatusCode is the HTTP response status code. Example: 404
	HTTPResponseStatusCode = "http.response.status_code"
	// HTTPVersion is the HTTP version. Example: 1.1
	HTTPVersion = "http.version"
)

// Log Fields
// Details about the event’s logging mechanism or logging transport. The Log
// fields are typically populated with details about the logging origin. The
// details specific to your event source are typically not logged as Log* fields,
// but rather in event.* or in other ECS fields.

const (
	// LogLevel is the original log level of the log event. If the source of the
	// event provides a log level or textual severity, this is the one that goes
	// in LogLevel. If your source doesn’t specify one, you may put your event
	// transport’s severity here (e.g. Syslog severity). Some examples are warn,
	// err, i, informational. Example: error
	LogLevel = "log.level"
	// LogLogger is the name of the logger inside an application. This is
	// usually the name of the class which initialized the logger, or can be a
	// custom name. Example: org.elasticsearch.bootstrap.Bootstrap
	LogLogger = "log.logger"
	// LogOriginFileLine is the line number of the file containing the source
	// code which originated the log event. Example: 42
	// See https://pkg.go.dev/runtime#Caller
	LogOriginFileLine = "log.origin.file.line"
	// LogOriginFileName is the name of the file containing the source code
	// which originated the log event. Note that this field is not meant to
	// capture the log file. Example: Bootstrap.java
	// See https://pkg.go.dev/runtime#Caller
	LogOriginFileName = "log.origin.file.name"
	// LogOriginFunction is the name of the function or method which originated
	// the log event. Example: init
	// See https://pkg.go.dev/runtime#Frames
	LogOriginFunction = "log.origin.function"
)

// URL Fields
// URL fields provide support for complete or partial URLs, and supports the
// breaking down into scheme, domain, path, and so on.
const (
	// URLDomain is the domain of the url, such as "www.elastic.co". In some
	// cases a URL may refer to an IP and/or port directly, without a domain
	// name. In this case, the IP address would go to the domain field. If the
	// URL contains a literal IPv6 address enclosed by [ and ] (IETF RFC 2732),
	// the [ and ] characters should also be captured in the domain field.
	// Example: www.elastic.co
	URLDomain = "url.domain"
	// URLExtension is the field contains the file extension from the original
	// request url, excluding the leading dot. The file extension is only set if
	// it exists, as not every url has a file extension. The leading period must
	// not be included. For example, the value must be "png", not ".png". Note
	// that when the file name has multiple extensions (example.tar.gz), only
	// the last one should be captured ("gz", not "tar.gz"). Example: png
	URLExtension = "url.extension"
	// URLFragment is the portion of the url after the #, such as "top". The #
	// is not part of the fragment.
	URLFragment = "url.fragment"
	// URLFull If full URLs are important to your use case, they should be
	// stored in url.full, whether this field is reconstructed or present in the
	// event source. Example: https://www.elastic.co:443/search?q=es#top
	URLFull = "url.full"
	// URLOriginal is the unmodified original url as seen in the event source.
	// Note that in network monitoring, the observed URL may be a full URL,
	// whereas in access logs, the URL is often just represented as a path. This
	// field is meant to represent the URL as it was observed, complete or not.
	URLOriginal = "url.original"
	// URLPath is the path of the request, such as "/search".
	URLPath = "url.path"
	// URLPort is the port of the request, such as 443. Example: 443
	URLPort = "url.port"
	// URLQuery is the query field describes the query string of the request,
	// such as "q=elasticsearch". The ? is excluded from the query string. If a
	// URL contains no ?, there is no query field. If there is a ? but no query,
	// the query field exists with an empty string. The exists query can be used
	// to differentiate between the two cases.
	URLQuery = "url.query"
	// URLRegisteredDomain is the highest registered url domain, stripped of the
	// subdomain. For example, the registered domain for "foo.example.com" is
	// "example.com". This value can be determined precisely with a list like
	// the public suffix list (http://publicsuffix.org). Trying to approximate
	// this by simply taking the last two labels will not work well for TLDs
	// such as "co.uk". Example: example.com
	URLRegisteredDomain = "url.registered_domain"
	// URLScheme is the scheme of the request, such as "https". Note: The : is
	// not part of the scheme. Example: https
	URLScheme = "url.scheme"
	// URLSubdomain is the subdomain portion of a fully qualified domain name
	// includes all of the names except the host name under the
	// registered_domain. In a partially qualified domain, or if the the
	// qualification level of the full name cannot be determined, subdomain
	// contains all of the names below the registered domain.
	// For example the subdomain portion of "www.east.mydomain.co.uk" is "east".
	// If the domain has multiple levels of subdomain, such as
	// "sub2.sub1.example.com", the subdomain field should contain "sub2.sub1",
	// with no trailing period.
	URLSubdomain = "url.subdomain"
	// URLTopLevelDomain is the effective top level domain (eTLD), also known as
	// the domain suffix, is the last part of the domain name. For example, the
	// top level domain for example.com is "com". This value can be determined
	// precisely with a list like the public suffix list
	// (http://publicsuffix.org). Trying to approximate this by simply taking
	// the last label will not work well for effective TLDs such as "co.uk".
	// Example: co.uk
	URLTopLevelDomain = "url.top_level_domain"
)

// Service Fields
// The service fields describe the service for or from which the data was
// collected. These fields help you find and correlate logs for a specific
// service and version.
const (
	// ServiceAddress is the address where data about this service was collected
	// from. This should be a URI, network address (ipv4:port or [ipv6]:port) or
	// a resource path (sockets). Example: 172.26.0.2:5432
	ServiceAddress = "service.address"
	// ServiceEnvironment identifies the environment where the service is
	// running. If the same service runs in different environments (production,
	// staging, QA, development, etc.), the environment can identify other
	// instances of the same service. Can also group services and applications
	// from the same environment. Example: production
	ServiceEnvironment = "service.environment"
	// ServiceEphemeralID is the ephemeral identifier of this service (if one
	// exists). This id normally changes across restarts, but service.id does
	// not. Example: 8a4f500f
	ServiceEphemeralID = "service.ephemeral_id"
	// ServiceID is the unique identifier of the running service. If the service
	// is comprised of many nodes, the service.id should be the same for all
	// nodes. This id should uniquely identify the service. This makes it
	// possible to correlate logs and metrics for one specific service, no
	// matter which particular node emitted the event. Note that if you need to
	// see the events from one specific host of the service, you should filter
	// on that HostName or HostID instead. Example: d37e5ebfe0ae6c4972dbe
	ServiceID = "service.id"
	// ServiceName is the name of the service data is collected from. The name
	// of the service is normally user given. This allows for distributed
	// services that run on multiple hosts to correlate the related instances
	// based on the name. In the case of Elasticsearch the ServiceName could
	// contain the cluster name. For Beats the ServiceName is by default a copy
	// of the ServiceType field if no name is specified.
	// Example: elasticsearch-metrics
	ServiceName = "service.name"
	// ServiceNodeName is the name of a service node. This allows for two nodes
	// of the same service running on the same host to be differentiated.
	// Therefore, service.node.name should typically be unique across nodes of a
	// given service. In the case of Elasticsearch, the ServiceNodeName could
	// contain the unique node name within the Elasticsearch cluster. In cases
	// where the service doesn’t have the concept of a node name, the host name
	// or container name can be used to distinguish running instances that make
	// up this service. If those do not provide uniqueness (e.g. multiple
	// instances of the service running on the same host) - the node name can be
	// manually set. Example: instance-0000000016
	ServiceNodeName = "service.node.name"
	// ServiceState is the current state of the service.
	ServiceState = "service.state"
	// ServiceType is the type of the service data is collected from. The type
	// can be used to group and correlate logs and metrics from one service type.
	// Example: If logs or metrics are collected from Elasticsearch,
	// ServiceType would be elasticsearch. Example: elasticsearch
	ServiceType = "service.type"
	// ServiceVersion is the version of the service the data was collected from.
	// This allows to look at a data set only for a specific version of a
	// service. Example: 3.2.4
	ServiceVersion = "service.version"
)

// Source Fields
// Source fields capture details about the sender of a network exchange/packet.
// These fields are populated from a network event, packet, or other event
// containing details of a network transaction.
// Source fields are usually populated in conjunction with destination fields.
// The source and destination fields are considered the baseline and should
// always be filled if an event contains source and destination details from a
// network transaction. If the event also contains identification of the client
// and server roles, then the client and server fields should also be populated.
const (
	// SourceAddress is some event source addresses are defined ambiguously. The
	// event will sometimes list an IP, a domain or a unix socket. You should
	// always store the raw address in the SourceAddress field. Then it should
	// be duplicated to SourceIP or SourceDomain, depending on which one it is.
	SourceAddress = "source.address"
	// SourceBytes is the number of bytes sent from the source to the
	// destination.
	SourceBytes = "source.bytes"
	// SourceDomain is the source domain.
	SourceDomain = "source.domain"
	// SourceIP is the IP address of the source (IPv4 or IPv6).
	SourceIP = "source.ip"
	// SourcePort is the port of the source.
	SourcePort = "source.port"
	// SourceRegisteredDomain is the highest registered source domain, stripped
	// of the subdomain. For example, the registered domain for "foo.example.com"
	// is "example.com". This value can be determined precisely with a list like
	// the public suffix list (http://publicsuffix.org). Trying to approximate
	// this by simply taking the last two labels will not work well for TLDs
	// such as "co.uk". Example: example.com
	SourceRegisteredDomain = "source.registered_domain"
	// SourceSubdomain is the subdomain portion of a fully qualified domain name
	// includes all of the names except the host name under the
	// registered_domain. In a partially qualified domain, or if the the
	// qualification level of the full name cannot be determined, subdomain
	// contains all of the names below the registered domain. For example the
	// subdomain portion of "www.east.mydomain.co.uk" is "east". If the domain
	// has multiple levels of subdomain, such as "sub2.sub1.example.com", the
	// subdomain field should contain "sub2.sub1", with no trailing period.
	SourceSubdomain = "source.subdomain"
	// SourceTopLevelDomain is the effective top level domain (eTLD), also known
	// as the domain suffix, is the last part of the domain name. For example,
	// the top level domain for example.com is "com". This value can be
	// determined precisely with a list like the public suffix list
	// (http://publicsuffix.org). Trying to approximate this by simply taking
	// the last label will not work well for effective TLDs such as "co.uk".
	// Example: co.uk
	SourceTopLevelDomain = "source.top_level_domain"
)

// Tracing Fields
// Distributed tracing makes it possible to analyze performance throughout a
// microservice architecture all in one view. This is accomplished by tracing
// all of the requests - from the initial web request in the front-end service
// - to queries made through multiple back-end services.
const (
	// TracingSpanID is the unique identifier of the span within the scope of
	// its trace. A span represents an operation within a transaction, such as a
	// request to another service, or a database query.
	// Example: 3ff9a8981b7ccd5a
	TracingSpanID = "span.id"
	// TracingTraceID is unique identifier of the trace. A trace groups multiple
	// events like transactions that belong together. For example, a user
	// request handled by multiple inter-connected services.
	// Example: 4bf92f3577b34da6a3ce929d0e0e4736
	TracingTraceID = "trace.id"
	// TracingTransactionID is the unique identifier of the transaction within
	// the scope of its trace. A transaction is the highest level of work
	// measured within a service, such as a request to a server.
	// Example: 00f067aa0ba902b7
	TracingTransactionID = "transaction.id"
)

const (
	// AccountIDKey is the field name for the account ID in the logs
	AccountIDKey = "account"
	// ClientIDKey is the field name for the jwt azp / "basicAuth" user in the logs
	ClientIDKey = "ClientID"
	// EndPointKey is the field name for API end-point that emitted the log
	EndPointKey = "endpoint"
	// EventKey is the field name for common event-specific log entries
	EventKey = "event"
	// HTTPMethodKey is the field name for the HTTP request method in the logs
	HTTPMethodKey = "method"
	// NodeNameKey is the field name for the host / node name in the logs
	NodeNameKey = "nodeName"
	// PipelineIDKey is the field name for the pipeline ID in the logs
	PipelineIDKey = "pipeline"
	// TraceIDKey is the field name for the trace ID in the logs
	TraceIDKey = "TraceID"
	// BeamVersionKey is the field name for the current Beam version in the logs
	BeamVersionKey = "version"
	// OCRRequestKey is the field name for identifying requests with an active
	// OCR component
	OCRRequestKey = "ocrRequest"
)
