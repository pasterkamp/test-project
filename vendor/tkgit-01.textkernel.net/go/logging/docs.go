/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

// Package logging provides convenient initialization and wrapper functions for
// the sirupsen/logrus module.
//
// A full list of Elastic Common Schema (ECS) compatible log field constants is
// also available. For more information on ECS visit:
// https://www.elastic.co/guide/en/ecs/current/ecs-field-reference.html
package logging

//go:generate gomarkdoc --output docs/usage.md
