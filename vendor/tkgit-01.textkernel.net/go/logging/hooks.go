/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package logging

import (
	"context"
	"fmt"
	"net"
	"os"
	"regexp"
	"time"

	"github.com/beaubrewer/logrus_firehose"
	logrustash "github.com/bshuster-repo/logrus-logstash-hook"
	gogap "github.com/gogap/config"
	"github.com/gogap/logrus_mate"
	"github.com/sirupsen/logrus"
	"tkgit-01.textkernel.net/go/logging/internal/prometheus"
)

func init() {
	defer func() {
		// Allow gradual adoption of go/logging while the project still
		// initializes logrus hooks directly.
		if r := recover(); r != nil {
			fmt.Printf("Warning: recovered panic: %v\n", r)
		}
	}()
	logrus_mate.RegisterHook("firehose", firehoseHook)
	logrus_mate.RegisterHook("logstash", logstashHook)
	logrus_mate.RegisterHook("prometheus", prometheusHook)
}

func firehoseHook(config gogap.Configuration) (hook logrus.Hook, err error) {
	fh, err := logrus_firehose.New(config.GetString("stream"), logrus_firehose.Config{
		AccessKey: getConfig(config, "accessKey"),
		SecretKey: getConfig(config, "secretKey"),
		Region:    getConfig(config, "region"),
		Endpoint:  getConfig(config, "endpoint"),
	})
	if err != nil {
		fmt.Printf("[ERR] Firehose: Failed to create hook: %v\n", err)
		return fh, err
	}

	fh.AddNewLine(true)
	if config.GetBoolean("async", false) {
		fh.Async()
	}

	return fh, nil
}

func logstashHook(config gogap.Configuration) (logrus.Hook, error) {
	var endpoint = getConfig(config, "endpoint")

	if endpoint == "" {
		fmt.Printf("[WARN] Logstash: No endpoint defined\n")
		return nil, nil
	}

	conn, err := connectLoop(endpoint, 5*time.Second)
	if err != nil {
		fmt.Printf("[ERR] Logstash: Cannot contact endpoint: %v\n", err)
		return nil, err
	}

	hook := logrustash.New(conn, logrustash.DefaultFormatter(logrus.Fields(logFields)))
	return hook, nil
}

func connectLoop(endpoint string, timeout time.Duration) (conn net.Conn, err error) {
	deadline, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	ticker := time.NewTicker(50 * time.Millisecond)
	defer ticker.Stop()

	for {
		select {
		case <-deadline.Done():
			return nil, fmt.Errorf("connection failed after %s timeout", timeout)

		case <-ticker.C:
			conn, err = net.Dial("tcp", endpoint)
			if err == nil {
				return
			}
		}
	}
}

func prometheusHook(config gogap.Configuration) (logrus.Hook, error) {
	logger := getConfig(config, "logger")
	fields := regexp.MustCompile(`\s*,\s*`).Split(getConfig(config, "labels"), -1)

	return prometheus.NewHook(logger, fields)
}

// getConfig either returns the configured value verbatim, or returns the
// environment variable should the value start with ENV:YOUR_VAR.
func getConfig(config gogap.Configuration, key string) string {
	val := config.GetString(key)
	if len(val) > 4 && val[:4] == "ENV:" {
		return os.Getenv(val[4:])
	}
	return val
}
