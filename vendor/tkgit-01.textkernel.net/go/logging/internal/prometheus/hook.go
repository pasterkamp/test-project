/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package prometheus

import (
	"fmt"
	"regexp"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

// permitted is a regexp matching all characters allowed in a prometheus metric
// label.
var permitted = regexp.MustCompile("[^a-z0-9]+")

// NewHook creates a Prometheus counter to keep track of log message frequency
// per level, and possibly other fields available in the log entry. Each hook
// can have a non-empty name to track metrics for different loggers.
//
// Note: the number of extra fields and their possible values should be kept low
// as it impacts Prometheus performance. The fields can also only contain string
// values for now.
func NewHook(logger string, fields []string) (*Hook, error) {
	labelMapping := map[string]string{}
	labels := make([]string, len(fields)+1)
	labels[0] = "level"
	for i, field := range fields {
		label := permitted.ReplaceAllString(field, "_")
		labelMapping[field] = label
		labels[i+1] = label
	}

	name := "go_logging_messages_total"
	help := "Total number of log messages."
	if logger != "" {
		name = fmt.Sprintf("go_logging_%s_messages_total", logger)
		help = fmt.Sprintf("Total number of log messages for the %q logger.", logger)
	}

	counterVec := prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: name,
		Help: help,
	}, labels)

	prometheus.Unregister(counterVec)

	err := prometheus.Register(counterVec)
	if err != nil {
		return nil, fmt.Errorf("%w: %w", ErrFailedToRegister, err)
	}

	return &Hook{
		counterVec: counterVec,
		labels:     labelMapping,
	}, nil
}

// Levels returns all supported log levels, i.e.: Debug, Info, Warn and Error, as
// there is no point incrementing a counter just before exiting/panicking.
func (h *Hook) Levels() []logrus.Level {
	return logrus.AllLevels
}
