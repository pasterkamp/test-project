/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package prometheus

import (
	"regexp"

	"github.com/prometheus/client_golang/prometheus"
	dto "github.com/prometheus/client_model/go"
)

var reWhitespace = regexp.MustCompile(`\s\s+`)

// Collect returns the current metrics collected by the Prometheus hook. Only
// really useful for testing and debugging scenarios.
func (h *Hook) Collect() []string {
	c := make(chan prometheus.Metric)

	go func(c chan prometheus.Metric) {
		h.counterVec.Collect(c)
		close(c)
	}(c)

	metric := dto.Metric{}

	metrics := []string{}
	for m := range c {
		_ = m.Write(&metric)
		metrics = append(metrics, reWhitespace.ReplaceAllString(metric.String(), " "))
	}

	return metrics
}
