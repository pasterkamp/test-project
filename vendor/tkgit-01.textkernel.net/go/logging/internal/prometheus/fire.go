/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

// Fire increments the Prometheus counter for the log level, and any potential
// fields.
func (h *Hook) Fire(entry *logrus.Entry) error {
	labels := prometheus.Labels{
		"level": entry.Level.String(),
	}

	for field, label := range h.labels {
		if value, ok := entry.Data[field].(string); ok {
			labels[label] = value
		} else {
			labels[label] = ""
		}
	}

	h.counterVec.With(labels).Inc()

	return nil
}
