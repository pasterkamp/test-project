/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
)

// Hook implements the logrus Hook interface, and contains a Prometheus counter.
type Hook struct {
	// counterVec is the Prometheus counter
	counterVec *prometheus.CounterVec
	// labels is an optional list of additional fields
	labels map[string]string
}
