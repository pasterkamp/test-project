/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package logging

import (
	"os"

	"github.com/gogap/logrus_mate"
	log "github.com/sirupsen/logrus"
)

// Init will configure the logging using the configuration provided in cfgPath.
func Init(cfgPath string) error {
	var options []logrus_mate.Option
	if _, err := os.Stat(cfgPath); err != nil {
		return err
	}

	options = append(options, logrus_mate.ConfigFile(cfgPath))
	var err error
	mate, err = logrus_mate.NewLogrusMate(options...)
	if err != nil {
		return err
	}

	return mate.Hijack(log.StandardLogger(), "root")
}

// AddFields will inject a set of static fields to all loggers. Any value not of
// type bool, int, or string is considered a "context field". These fields will
// be populated from context - either a direct context using WithContextFields,
// or as provided in the http.Request in WithRequestFields.
func AddFields(f map[string]interface{}) {
	for k, v := range f {
		switch v.(type) {
		case bool:
			logFields[k] = v
		case int:
			logFields[k] = v
		case string:
			logFields[k] = v
		default:
			ctxFields[k] = v
		}
	}
}
