/*************************************************************************
 *
 * Textkernel CONFIDENTIAL
 * __________________
 *
 *  Textkernel BV
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Textkernel BV and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Textkernel BV
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Textkernel BV.
 *
 */

package logging

import (
	"github.com/sirupsen/logrus"
)

// Fields is a retyping of sirupsen/logrus.Fields, so it can be used
// interchangeably
type Fields logrus.Fields

// Entry is a retyping of sirupsen/logrus.Entry, so it can be used
// interchangeably, and extended.
type Entry struct {
	*logrus.Entry
}
